   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.13 - 05 Feb 2019
   3                     ; Generator (Limited) V4.4.9 - 06 Feb 2019
   4                     ; Optimizer V4.4.9 - 06 Feb 2019
  50                     ; 53 INTERRUPT_HANDLER(NonHandledInterrupt, 25)
  50                     ; 54 {
  51                     .text:	section	.text,new
  52  0000               f_NonHandledInterrupt:
  56                     ; 58 }
  59  0000 80            	iret	
  81                     ; 66 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  81                     ; 67 {
  82                     .text:	section	.text,new
  83  0000               f_TRAP_IRQHandler:
  87                     ; 71 }
  90  0000 80            	iret	
 112                     ; 77 INTERRUPT_HANDLER(TLI_IRQHandler, 0)
 112                     ; 78 {
 113                     .text:	section	.text,new
 114  0000               f_TLI_IRQHandler:
 118                     ; 82 }
 121  0000 80            	iret	
 143                     ; 89 INTERRUPT_HANDLER(AWU_IRQHandler, 1)
 143                     ; 90 {
 144                     .text:	section	.text,new
 145  0000               f_AWU_IRQHandler:
 149                     ; 94 }
 152  0000 80            	iret	
 174                     ; 101 INTERRUPT_HANDLER(CLK_IRQHandler, 2)
 174                     ; 102 {
 175                     .text:	section	.text,new
 176  0000               f_CLK_IRQHandler:
 180                     ; 106 }
 183  0000 80            	iret	
 206                     ; 113 INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
 206                     ; 114 {
 207                     .text:	section	.text,new
 208  0000               f_EXTI_PORTA_IRQHandler:
 212                     ; 118 }
 215  0000 80            	iret	
 238                     ; 125 INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
 238                     ; 126 {
 239                     .text:	section	.text,new
 240  0000               f_EXTI_PORTB_IRQHandler:
 244                     ; 130 }
 247  0000 80            	iret	
 270                     ; 137 INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
 270                     ; 138 {
 271                     .text:	section	.text,new
 272  0000               f_EXTI_PORTC_IRQHandler:
 276                     ; 142 }
 279  0000 80            	iret	
 302                     ; 149 INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
 302                     ; 150 {
 303                     .text:	section	.text,new
 304  0000               f_EXTI_PORTD_IRQHandler:
 308                     ; 154 }
 311  0000 80            	iret	
 334                     ; 161 INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
 334                     ; 162 {
 335                     .text:	section	.text,new
 336  0000               f_EXTI_PORTE_IRQHandler:
 340                     ; 166 }
 343  0000 80            	iret	
 365                     ; 212 INTERRUPT_HANDLER(SPI_IRQHandler, 10)
 365                     ; 213 {
 366                     .text:	section	.text,new
 367  0000               f_SPI_IRQHandler:
 371                     ; 217 }
 374  0000 80            	iret	
 397                     ; 224 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
 397                     ; 225 {
 398                     .text:	section	.text,new
 399  0000               f_TIM1_UPD_OVF_TRG_BRK_IRQHandler:
 403                     ; 229 }
 406  0000 80            	iret	
 429                     ; 236 INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
 429                     ; 237 {
 430                     .text:	section	.text,new
 431  0000               f_TIM1_CAP_COM_IRQHandler:
 435                     ; 241 }
 438  0000 80            	iret	
 461                     ; 273  INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
 461                     ; 274 {
 462                     .text:	section	.text,new
 463  0000               f_TIM2_UPD_OVF_BRK_IRQHandler:
 467                     ; 278 }
 470  0000 80            	iret	
 493                     ; 285  INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
 493                     ; 286 {
 494                     .text:	section	.text,new
 495  0000               f_TIM2_CAP_COM_IRQHandler:
 499                     ; 290 }
 502  0000 80            	iret	
 525                     ; 327  INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
 525                     ; 328 {
 526                     .text:	section	.text,new
 527  0000               f_UART1_TX_IRQHandler:
 531                     ; 332 }
 534  0000 80            	iret	
 557                     ; 339  INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
 557                     ; 340 {
 558                     .text:	section	.text,new
 559  0000               f_UART1_RX_IRQHandler:
 563                     ; 344 }
 566  0000 80            	iret	
 588                     ; 352 INTERRUPT_HANDLER(I2C_IRQHandler, 19)
 588                     ; 353 {
 589                     .text:	section	.text,new
 590  0000               f_I2C_IRQHandler:
 594                     ; 357 }
 597  0000 80            	iret	
 619                     ; 432  INTERRUPT_HANDLER(ADC1_IRQHandler, 22)
 619                     ; 433 {
 620                     .text:	section	.text,new
 621  0000               f_ADC1_IRQHandler:
 625                     ; 438     return;
 628  0000 80            	iret	
 653                     ; 461  INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
 653                     ; 462 {
 654                     .text:	section	.text,new
 655  0000               f_TIM4_UPD_OVF_IRQHandler:
 657  0000 8a            	push	cc
 658  0001 84            	pop	a
 659  0002 a4bf          	and	a,#191
 660  0004 88            	push	a
 661  0005 86            	pop	cc
 662  0006 3b0002        	push	c_x+2
 663  0009 be00          	ldw	x,c_x
 664  000b 89            	pushw	x
 665  000c 3b0002        	push	c_y+2
 666  000f be00          	ldw	x,c_y
 667  0011 89            	pushw	x
 670                     ; 463    TimingDelay_Decrement();
 672  0012 cd0000        	call	_TimingDelay_Decrement
 674                     ; 465   TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
 676  0015 a601          	ld	a,#1
 677  0017 cd0000        	call	_TIM4_ClearITPendingBit
 679                     ; 467 }
 682  001a 85            	popw	x
 683  001b bf00          	ldw	c_y,x
 684  001d 320002        	pop	c_y+2
 685  0020 85            	popw	x
 686  0021 bf00          	ldw	c_x,x
 687  0023 320002        	pop	c_x+2
 688  0026 80            	iret	
 711                     ; 475 INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
 711                     ; 476 {
 712                     .text:	section	.text,new
 713  0000               f_EEPROM_EEC_IRQHandler:
 717                     ; 480 }
 720  0000 80            	iret	
 732                     	xref	_TimingDelay_Decrement
 733                     	xdef	f_EEPROM_EEC_IRQHandler
 734                     	xdef	f_TIM4_UPD_OVF_IRQHandler
 735                     	xdef	f_ADC1_IRQHandler
 736                     	xdef	f_I2C_IRQHandler
 737                     	xdef	f_UART1_RX_IRQHandler
 738                     	xdef	f_UART1_TX_IRQHandler
 739                     	xdef	f_TIM2_CAP_COM_IRQHandler
 740                     	xdef	f_TIM2_UPD_OVF_BRK_IRQHandler
 741                     	xdef	f_TIM1_UPD_OVF_TRG_BRK_IRQHandler
 742                     	xdef	f_TIM1_CAP_COM_IRQHandler
 743                     	xdef	f_SPI_IRQHandler
 744                     	xdef	f_EXTI_PORTE_IRQHandler
 745                     	xdef	f_EXTI_PORTD_IRQHandler
 746                     	xdef	f_EXTI_PORTC_IRQHandler
 747                     	xdef	f_EXTI_PORTB_IRQHandler
 748                     	xdef	f_EXTI_PORTA_IRQHandler
 749                     	xdef	f_CLK_IRQHandler
 750                     	xdef	f_AWU_IRQHandler
 751                     	xdef	f_TLI_IRQHandler
 752                     	xdef	f_TRAP_IRQHandler
 753                     	xdef	f_NonHandledInterrupt
 754                     	xref	_TIM4_ClearITPendingBit
 755                     	xref.b	c_x
 756                     	xref.b	c_y
 775                     	end
