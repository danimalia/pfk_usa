   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.13 - 05 Feb 2019
   3                     ; Generator (Limited) V4.4.9 - 06 Feb 2019
   4                     ; Optimizer V4.4.9 - 06 Feb 2019
  21                     	bsct
  22  0000               _TimingDelay:
  23  0000 00000514      	dc.l	1300
  24  0004               _flash_count:
  25  0004 00            	dc.b	0
  26  0005               _StatoIn1:
  27  0005 03            	dc.b	3
  28  0006               _In1_0:
  29  0006 0000          	dc.w	0
  30  0008               _In1_1:
  31  0008 0003          	dc.w	3
  32  000a               _Debounce:
  33  000a 00fa          	dc.w	250
  34  000c               _input1:
  35  000c 00            	dc.b	0
 103                     ; 72 void main(void)
 103                     ; 73 {
 105                     .text:	section	.text,new
 106  0000               _main:
 108  0000 5206          	subw	sp,#6
 109       00000006      OFST:	set	6
 112                     ; 75     uint8_t val = 0x00, val_comp = 0x00;
 116                     ; 76     uint32_t add = 0x00;
 118                     ; 79     FLASH_SetProgrammingTime(FLASH_PROGRAMTIME_STANDARD);
 120  0002 4f            	clr	a
 121  0003 cd0000        	call	_FLASH_SetProgrammingTime
 123                     ; 82     FLASH_Unlock(FLASH_MEMTYPE_DATA);
 125  0006 a6f7          	ld	a,#247
 126  0008 cd0000        	call	_FLASH_Unlock
 128                     ; 84 		flash_count = FLASH_ReadByte(0x40A5);
 130  000b ae40a5        	ldw	x,#16549
 131  000e 89            	pushw	x
 132  000f 5f            	clrw	x
 133  0010 89            	pushw	x
 134  0011 cd0000        	call	_FLASH_ReadByte
 136  0014 5b04          	addw	sp,#4
 137  0016 b704          	ld	_flash_count,a
 138                     ; 87     OperationStatus = PASSED;
 140  0018 35010000      	mov	_OperationStatus,#1
 141                     ; 92   CLK_Config();  
 143  001c cd0000        	call	L3_CLK_Config
 145                     ; 95   GPIO_Config();  
 147  001f cd0000        	call	L7_GPIO_Config
 149                     ; 98   TIM4_Config();    
 151  0022 cd0000        	call	L5_TIM4_Config
 153  0025               L74:
 154                     ; 102 		UpdateLEDs();
 156  0025 cd0000        	call	L11_UpdateLEDs
 158                     ; 103 		UpdateSwitch();
 160  0028 cd0000        	call	_UpdateSwitch
 163  002b 20f8          	jra	L74
 201                     .const:	section	.text
 202  0000               L03:
 203  0000 00000514      	dc.l	1300
 204  0004               L43:
 205  0004 00000492      	dc.l	1170
 206  0008               L04:
 207  0008 00000410      	dc.l	1040
 208  000c               L44:
 209  000c 0000038e      	dc.l	910
 210  0010               L05:
 211  0010 0000030c      	dc.l	780
 212  0014               L45:
 213  0014 0000028a      	dc.l	650
 214  0018               L06:
 215  0018 00000208      	dc.l	520
 216  001c               L46:
 217  001c 00000186      	dc.l	390
 218  0020               L27:
 219  0020 000004e2      	dc.l	1250
 220  0024               L67:
 221  0024 000004b0      	dc.l	1200
 222  0028               L201:
 223  0028 0000047e      	dc.l	1150
 224  002c               L601:
 225  002c 0000044c      	dc.l	1100
 226  0030               L211:
 227  0030 0000041a      	dc.l	1050
 228  0034               L611:
 229  0034 000003e8      	dc.l	1000
 230  0038               L221:
 231  0038 000003b6      	dc.l	950
 232  003c               L621:
 233  003c 00000384      	dc.l	900
 234  0040               L231:
 235  0040 00000352      	dc.l	850
 236  0044               L631:
 237  0044 00000320      	dc.l	800
 238  0048               L241:
 239  0048 000002ee      	dc.l	750
 240  004c               L002:
 241  004c 000004ba      	dc.l	1210
 242  0050               L602:
 243  0050 00000460      	dc.l	1120
 244  0054               L222:
 245  0054 000004c9      	dc.l	1225
 246  0058               L032:
 247  0058 00000433      	dc.l	1075
 248  005c               L632:
 249  005c 0000039d      	dc.l	925
 250  0060               L442:
 251  0060 00000307      	dc.l	775
 252  0064               L052:
 253  0064 000002bc      	dc.l	700
 254  0068               L462:
 255  0068 00000226      	dc.l	550
 256  006c               L072:
 257  006c 00000190      	dc.l	400
 258  0070               L472:
 259  0070 000000fa      	dc.l	250
 260  0074               L003:
 261  0074 00000064      	dc.l	100
 262                     ; 107 static void UpdateLEDs(void)
 262                     ; 108 {
 263                     .text:	section	.text,new
 264  0000               L11_UpdateLEDs:
 266  0000 5204          	subw	sp,#4
 267       00000004      OFST:	set	4
 270                     ; 109 	unsigned long currentTick = TimingDelay;
 272  0002 ae0000        	ldw	x,#_TimingDelay
 273  0005 cd0000        	call	c_ltor
 275                     ; 111 	switch (flash_count)
 277  0008 b604          	ld	a,_flash_count
 279                     ; 290 			default: break;		
 280  000a 272d          	jreq	L35
 281  000c 4a            	dec	a
 282  000d 2603cc00af    	jreq	L55
 283  0012 4a            	dec	a
 284  0013 2603cc0117    	jreq	L75
 285  0018 4a            	dec	a
 286  0019 2603cc0137    	jreq	L16
 287  001e 4a            	dec	a
 288  001f 2603cc015e    	jreq	L36
 289  0024 4a            	dec	a
 290  0025 2603cc0189    	jreq	L56
 291  002a 4a            	dec	a
 292  002b 2603cc0199    	jreq	L76
 293  0030 4a            	dec	a
 294  0031 2603cc0236    	jreq	L17
 295  0036 cc02d4        	jra	L511
 296  0039               L35:
 297                     ; 114 			case 0: 
 297                     ; 115 			if (TimingDelay == TOTAL_PERIOD) 		GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 299  0039 cd02d7        	call	LC017
 304  003c 271c          	jreq	LC012
 305                     ; 116 			else if (TimingDelay == (TOTAL_PERIOD-130)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 307  003e ae0000        	ldw	x,#_TimingDelay
 308  0041 cd0000        	call	c_ltor
 310  0044 ae0004        	ldw	x,#L43
 311  0047 cd0000        	call	c_lcmp
 316  004a 2724          	jreq	LC016
 317                     ; 117 			else if (TimingDelay == (TOTAL_PERIOD-260)) GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 319  004c ae0000        	ldw	x,#_TimingDelay
 320  004f cd0000        	call	c_ltor
 322  0052 ae0008        	ldw	x,#L04
 323  0055 cd0000        	call	c_lcmp
 325  0058 2608          	jrne	L721
 328  005a               LC012:
 335  005a 4b08          	push	#8
 336  005c ae5000        	ldw	x,#20480
 339  005f cc0218        	jp	LC009
 340  0062               L721:
 341                     ; 118 			else if (TimingDelay == (TOTAL_PERIOD-390)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 343  0062 ae0000        	ldw	x,#_TimingDelay
 344  0065 cd0000        	call	c_ltor
 346  0068 ae000c        	ldw	x,#L44
 347  006b cd0000        	call	c_lcmp
 349  006e 2608          	jrne	L331
 352  0070               LC016:
 360  0070 4b08          	push	#8
 361  0072 ae5000        	ldw	x,#20480
 364  0075 cc012a        	jp	LC013
 365  0078               L331:
 366                     ; 119 			else if (TimingDelay == (TOTAL_PERIOD-520)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 368  0078 ae0000        	ldw	x,#_TimingDelay
 369  007b cd0000        	call	c_ltor
 371  007e ae0010        	ldw	x,#L05
 372  0081 cd0000        	call	c_lcmp
 377  0084 2603cc0213    	jreq	LC010
 378                     ; 120 			else if (TimingDelay == (TOTAL_PERIOD-650)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 380  0089 cd0302        	call	LC021
 385  008c 2603cc0125    	jreq	LC014
 386                     ; 121 			else if (TimingDelay == (TOTAL_PERIOD-780)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 388  0091 ae0000        	ldw	x,#_TimingDelay
 389  0094 cd0000        	call	c_ltor
 391  0097 ae0018        	ldw	x,#L06
 392  009a cd0000        	call	c_lcmp
 397  009d 27e7          	jreq	LC010
 398                     ; 122 			else if (TimingDelay == (TOTAL_PERIOD-910)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 400  009f ae0000        	ldw	x,#_TimingDelay
 401  00a2 cd0000        	call	c_ltor
 403  00a5 ae001c        	ldw	x,#L46
 404  00a8 cd0000        	call	c_lcmp
 406  00ab 2689          	jrne	L511
 409  00ad 2076          	jp	LC014
 410  00af               L55:
 411                     ; 126 			case 1: 
 411                     ; 127 			if (TimingDelay == TOTAL_PERIOD) 		GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 413  00af cd02d7        	call	LC017
 418  00b2 27a6          	jreq	LC012
 419                     ; 128 			else if (TimingDelay == (TOTAL_PERIOD-50)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 421  00b4 cd030e        	call	LC022
 426  00b7 27b7          	jreq	LC016
 427                     ; 129 			else if (TimingDelay == (TOTAL_PERIOD-100)) GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 429  00b9 cd031a        	call	LC023
 434  00bc 279c          	jreq	LC012
 435                     ; 130 			else if (TimingDelay == (TOTAL_PERIOD-150)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 437  00be cd02e0        	call	LC018
 442  00c1 27ad          	jreq	LC016
 443                     ; 131 			else if (TimingDelay == (TOTAL_PERIOD-200)) GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 445  00c3 cd0326        	call	LC024
 450  00c6 2792          	jreq	LC012
 451                     ; 132 			else if (TimingDelay == (TOTAL_PERIOD-250)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 453  00c8 cd0332        	call	LC025
 458  00cb 27a3          	jreq	LC016
 459                     ; 133 			else if (TimingDelay == (TOTAL_PERIOD-300)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 461  00cd cd02ec        	call	LC019
 462  00d0 cd0000        	call	c_lcmp
 467  00d3 27b1          	jreq	LC010
 468                     ; 134 			else if (TimingDelay == (TOTAL_PERIOD-350)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 470  00d5 ae0000        	ldw	x,#_TimingDelay
 471  00d8 cd0000        	call	c_ltor
 473  00db ae0038        	ldw	x,#L221
 474  00de cd0000        	call	c_lcmp
 479  00e1 2742          	jreq	LC014
 480                     ; 135 			else if (TimingDelay == (TOTAL_PERIOD-400)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 482  00e3 ae0000        	ldw	x,#_TimingDelay
 483  00e6 cd0000        	call	c_ltor
 485  00e9 ae003c        	ldw	x,#L621
 486  00ec cd0000        	call	c_lcmp
 491  00ef 2795          	jreq	LC010
 492                     ; 136 			else if (TimingDelay == (TOTAL_PERIOD-450)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 494  00f1 cd02f6        	call	LC020
 499  00f4 272f          	jreq	LC014
 500                     ; 137 			else if (TimingDelay == (TOTAL_PERIOD-500)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 502  00f6 ae0000        	ldw	x,#_TimingDelay
 503  00f9 cd0000        	call	c_ltor
 505  00fc ae0044        	ldw	x,#L631
 506  00ff cd0000        	call	c_lcmp
 511  0102 2782          	jreq	LC010
 512                     ; 138 			else if (TimingDelay == (TOTAL_PERIOD-550)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 514  0104 ae0000        	ldw	x,#_TimingDelay
 515  0107 cd0000        	call	c_ltor
 517  010a ae0048        	ldw	x,#L241
 518  010d cd0000        	call	c_lcmp
 520  0110 2703cc02d4    	jrne	L511
 523  0115 200e          	jp	LC014
 524  0117               L75:
 525                     ; 142 			case 2: 
 525                     ; 143 				if (TimingDelay == TOTAL_PERIOD) 		
 527  0117 cd02d7        	call	LC017
 529  011a 2614          	jrne	L332
 530                     ; 145 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 533  011c               LC015:
 537  011c 4b08          	push	#8
 538  011e ae5000        	ldw	x,#20480
 539  0121 cd0000        	call	_GPIO_WriteHigh
 540  0124 84            	pop	a
 541                     ; 146 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 543  0125               LC014:
 554  0125 4b08          	push	#8
 555  0127 ae500a        	ldw	x,#20490
 556  012a               LC013:
 557  012a cd0000        	call	_GPIO_WriteLow
 560  012d cc021b        	jp	LC008
 561  0130               L332:
 562                     ; 148 				else if (TimingDelay == (TOTAL_PERIOD-650)) 
 564  0130 cd0302        	call	LC021
 566  0133 26dd          	jrne	L511
 567                     ; 150 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 569                     ; 151 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 571  0135 203a          	jp	LC011
 572  0137               L16:
 573                     ; 156 			case 3: 
 573                     ; 157 				if (TimingDelay == TOTAL_PERIOD) 		
 575  0137 cd02d7        	call	LC017
 577                     ; 159 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 579                     ; 160 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 582  013a 27e0          	jreq	LC015
 583                     ; 162 				else if (TimingDelay == (TOTAL_PERIOD-50)) 
 585  013c cd030e        	call	LC022
 587                     ; 164 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 590  013f 2603cc0070    	jreq	LC016
 591                     ; 166 				else if (TimingDelay == (TOTAL_PERIOD-100)) 
 593  0144 cd031a        	call	LC023
 595                     ; 168 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 598  0147 2603cc005a    	jreq	LC012
 599                     ; 170 				else if (TimingDelay == (TOTAL_PERIOD-150)) 
 601  014c cd02e0        	call	LC018
 603                     ; 172 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 606  014f 27f0          	jreq	LC016
 607                     ; 174 				else if (TimingDelay == (TOTAL_PERIOD-200)) 
 609  0151 cd0326        	call	LC024
 611                     ; 176 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 614  0154 27f3          	jreq	LC012
 615                     ; 178 				else if (TimingDelay == (TOTAL_PERIOD-250)) 
 617  0156 cd0332        	call	LC025
 619  0159 26b7          	jrne	L511
 620                     ; 180 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 622  015b cc0070        	jp	LC016
 623  015e               L36:
 624                     ; 185 				case 4: 
 624                     ; 186 				if (TimingDelay == TOTAL_PERIOD) 		
 626  015e cd02d7        	call	LC017
 628                     ; 188 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 630                     ; 189 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 633  0161 27b9          	jreq	LC015
 634                     ; 191 				else if (TimingDelay == (TOTAL_PERIOD-90)) 
 636  0163 ae0000        	ldw	x,#_TimingDelay
 637  0166 cd0000        	call	c_ltor
 639  0169 ae004c        	ldw	x,#L002
 640  016c cd0000        	call	c_lcmp
 642  016f 260c          	jrne	L372
 643                     ; 193 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 646  0171               LC011:
 649  0171 4b08          	push	#8
 650  0173 ae5000        	ldw	x,#20480
 651  0176 cd0000        	call	_GPIO_WriteLow
 652  0179 84            	pop	a
 653                     ; 194 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 656  017a cc0213        	jp	LC010
 657  017d               L372:
 658                     ; 196 				else if (TimingDelay == (TOTAL_PERIOD-180)) TimingDelay = TOTAL_PERIOD;
 660  017d ae0000        	ldw	x,#_TimingDelay
 661  0180 cd0000        	call	c_ltor
 663  0183 ae0050        	ldw	x,#L602
 666  0186 cc02c7        	jp	LC007
 667  0189               L56:
 668                     ; 200 				case 5: 
 668                     ; 201 				if (TimingDelay == TOTAL_PERIOD) 		
 670  0189 cd02d7        	call	LC017
 672                     ; 203 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 674                     ; 204 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 677  018c 278e          	jreq	LC015
 678                     ; 206 				else if (TimingDelay == (TOTAL_PERIOD-150)) 
 680  018e cd02e0        	call	LC018
 682                     ; 208 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 684                     ; 209 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 687  0191 27de          	jreq	LC011
 688                     ; 211 				else if (TimingDelay == (TOTAL_PERIOD-300)) TimingDelay = TOTAL_PERIOD;
 690  0193 cd02ec        	call	LC019
 693  0196 cc02c7        	jp	LC007
 694  0199               L76:
 695                     ; 215 				case 6: 
 695                     ; 216 				if (TimingDelay == TOTAL_PERIOD) 		
 697  0199 cd02d7        	call	LC017
 699  019c 260a          	jrne	L313
 700                     ; 218 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 702  019e 4b08          	push	#8
 703  01a0 ae5000        	ldw	x,#20480
 704  01a3 cd0000        	call	_GPIO_WriteHigh
 707  01a6 2016          	jp	LC001
 708  01a8               L313:
 709                     ; 220 				else if (TimingDelay == (TOTAL_PERIOD-75)) 
 711  01a8 ae0000        	ldw	x,#_TimingDelay
 712  01ab cd0000        	call	c_ltor
 714  01ae ae0054        	ldw	x,#L222
 715  01b1 cd0000        	call	c_lcmp
 717  01b4 2609          	jrne	L513
 718                     ; 222 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 720  01b6 4b08          	push	#8
 721  01b8 ae5000        	ldw	x,#20480
 722  01bb cd0000        	call	_GPIO_WriteLow
 724  01be               LC001:
 725  01be 84            	pop	a
 726  01bf               L513:
 727                     ; 224 				if (TimingDelay == TOTAL_PERIOD-150) 		
 729  01bf cd02e0        	call	LC018
 731  01c2 260a          	jrne	L123
 732                     ; 226 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 734  01c4 4b08          	push	#8
 735  01c6 ae5000        	ldw	x,#20480
 736  01c9 cd0000        	call	_GPIO_WriteHigh
 739  01cc 2016          	jp	LC002
 740  01ce               L123:
 741                     ; 228 				else if (TimingDelay == (TOTAL_PERIOD-225)) 
 743  01ce ae0000        	ldw	x,#_TimingDelay
 744  01d1 cd0000        	call	c_ltor
 746  01d4 ae0058        	ldw	x,#L032
 747  01d7 cd0000        	call	c_lcmp
 749  01da 2609          	jrne	L323
 750                     ; 230 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 752  01dc 4b08          	push	#8
 753  01de ae5000        	ldw	x,#20480
 754  01e1 cd0000        	call	_GPIO_WriteLow
 756  01e4               LC002:
 757  01e4 84            	pop	a
 758  01e5               L323:
 759                     ; 232 				if (TimingDelay == TOTAL_PERIOD-300) 		
 761  01e5 cd02ec        	call	LC019
 762  01e8 cd0000        	call	c_lcmp
 764  01eb 260a          	jrne	L723
 765                     ; 234 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 767  01ed 4b08          	push	#8
 768  01ef ae500a        	ldw	x,#20490
 769  01f2 cd0000        	call	_GPIO_WriteHigh
 772  01f5 2016          	jp	LC003
 773  01f7               L723:
 774                     ; 236 				else if (TimingDelay == (TOTAL_PERIOD-375)) 
 776  01f7 ae0000        	ldw	x,#_TimingDelay
 777  01fa cd0000        	call	c_ltor
 779  01fd ae005c        	ldw	x,#L632
 780  0200 cd0000        	call	c_lcmp
 782  0203 2609          	jrne	L133
 783                     ; 238 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 785  0205 4b08          	push	#8
 786  0207 ae500a        	ldw	x,#20490
 787  020a cd0000        	call	_GPIO_WriteLow
 789  020d               LC003:
 790  020d 84            	pop	a
 791  020e               L133:
 792                     ; 240 				if (TimingDelay == TOTAL_PERIOD-450) 		
 794  020e cd02f6        	call	LC020
 796  0211 260c          	jrne	L533
 797                     ; 242 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 799  0213               LC010:
 809  0213 4b08          	push	#8
 810  0215 ae500a        	ldw	x,#20490
 811  0218               LC009:
 812  0218 cd0000        	call	_GPIO_WriteHigh
 814  021b               LC008:
 815  021b 84            	pop	a
 817  021c cc02d4        	jra	L511
 818  021f               L533:
 819                     ; 244 				else if (TimingDelay == (TOTAL_PERIOD-525)) 
 821  021f ae0000        	ldw	x,#_TimingDelay
 822  0222 cd0000        	call	c_ltor
 824  0225 ae0060        	ldw	x,#L442
 825  0228 cd0000        	call	c_lcmp
 827                     ; 246 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 830  022b 2603cc0125    	jreq	LC014
 831                     ; 248 				else if (TimingDelay == (TOTAL_PERIOD-600)) TimingDelay = TOTAL_PERIOD;
 833  0230 cd033e        	call	LC026
 836  0233 cc02c7        	jp	LC007
 837  0236               L17:
 838                     ; 253 				case 7: 
 838                     ; 254 				if (TimingDelay == TOTAL_PERIOD) 		
 840  0236 cd02d7        	call	LC017
 842  0239 260a          	jrne	L743
 843                     ; 256 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 845  023b 4b08          	push	#8
 846  023d ae5000        	ldw	x,#20480
 847  0240 cd0000        	call	_GPIO_WriteHigh
 850  0243 200d          	jp	LC004
 851  0245               L743:
 852                     ; 258 				else if (TimingDelay == (TOTAL_PERIOD-150)) 
 854  0245 cd02e0        	call	LC018
 856  0248 2609          	jrne	L153
 857                     ; 260 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 859  024a 4b08          	push	#8
 860  024c ae5000        	ldw	x,#20480
 861  024f cd0000        	call	_GPIO_WriteLow
 863  0252               LC004:
 864  0252 84            	pop	a
 865  0253               L153:
 866                     ; 262 				if (TimingDelay == TOTAL_PERIOD-300) 		
 868  0253 cd02ec        	call	LC019
 869  0256 cd0000        	call	c_lcmp
 871  0259 260a          	jrne	L553
 872                     ; 264 					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 874  025b 4b08          	push	#8
 875  025d ae5000        	ldw	x,#20480
 876  0260 cd0000        	call	_GPIO_WriteHigh
 879  0263 200d          	jp	LC005
 880  0265               L553:
 881                     ; 266 				else if (TimingDelay == (TOTAL_PERIOD-450)) 
 883  0265 cd02f6        	call	LC020
 885  0268 2609          	jrne	L753
 886                     ; 268 					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 888  026a 4b08          	push	#8
 889  026c ae5000        	ldw	x,#20480
 890  026f cd0000        	call	_GPIO_WriteLow
 892  0272               LC005:
 893  0272 84            	pop	a
 894  0273               L753:
 895                     ; 270 				if (TimingDelay == TOTAL_PERIOD-600) 		
 897  0273 cd033e        	call	LC026
 898  0276 cd0000        	call	c_lcmp
 900  0279 260a          	jrne	L363
 901                     ; 272 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 903  027b 4b08          	push	#8
 904  027d ae500a        	ldw	x,#20490
 905  0280 cd0000        	call	_GPIO_WriteHigh
 908  0283 2016          	jp	LC006
 909  0285               L363:
 910                     ; 274 				else if (TimingDelay == (TOTAL_PERIOD-750)) 
 912  0285 ae0000        	ldw	x,#_TimingDelay
 913  0288 cd0000        	call	c_ltor
 915  028b ae0068        	ldw	x,#L462
 916  028e cd0000        	call	c_lcmp
 918  0291 2609          	jrne	L563
 919                     ; 276 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 921  0293 4b08          	push	#8
 922  0295 ae500a        	ldw	x,#20490
 923  0298 cd0000        	call	_GPIO_WriteLow
 925  029b               LC006:
 926  029b 84            	pop	a
 927  029c               L563:
 928                     ; 278 				if (TimingDelay == TOTAL_PERIOD-900) 		
 930  029c ae0000        	ldw	x,#_TimingDelay
 931  029f cd0000        	call	c_ltor
 933  02a2 ae006c        	ldw	x,#L072
 934  02a5 cd0000        	call	c_lcmp
 936                     ; 280 					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 939  02a8 2603cc0213    	jreq	LC010
 940                     ; 282 				else if (TimingDelay == (TOTAL_PERIOD-1050)) 
 942  02ad ae0000        	ldw	x,#_TimingDelay
 943  02b0 cd0000        	call	c_ltor
 945  02b3 ae0070        	ldw	x,#L472
 946  02b6 cd0000        	call	c_lcmp
 948                     ; 284 					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
 951  02b9 2603cc0125    	jreq	LC014
 952                     ; 286 				else if (TimingDelay == (TOTAL_PERIOD-1200)) TimingDelay = TOTAL_PERIOD;
 954  02be ae0000        	ldw	x,#_TimingDelay
 955  02c1 cd0000        	call	c_ltor
 957  02c4 ae0074        	ldw	x,#L003
 961  02c7               LC007:
 962  02c7 cd0000        	call	c_lcmp
 963  02ca 2608          	jrne	L511
 967  02cc ae0514        	ldw	x,#1300
 968  02cf bf02          	ldw	_TimingDelay+2,x
 969  02d1 5f            	clrw	x
 970  02d2 bf00          	ldw	_TimingDelay,x
 971                     ; 290 			default: break;		
 974  02d4               L511:
 975                     ; 363 }
 978  02d4 5b04          	addw	sp,#4
 979  02d6 81            	ret	
 980  02d7               LC017:
 981  02d7 cd0000        	call	c_ltor
 983  02da ae0000        	ldw	x,#L03
 984  02dd cc0000        	jp	c_lcmp
 985  02e0               LC018:
 986  02e0 ae0000        	ldw	x,#_TimingDelay
 987  02e3 cd0000        	call	c_ltor
 989  02e6 ae0028        	ldw	x,#L201
 990  02e9 cc0000        	jp	c_lcmp
 991  02ec               LC019:
 992  02ec ae0000        	ldw	x,#_TimingDelay
 993  02ef cd0000        	call	c_ltor
 995  02f2 ae0034        	ldw	x,#L611
 996  02f5 81            	ret	
 997  02f6               LC020:
 998  02f6 ae0000        	ldw	x,#_TimingDelay
 999  02f9 cd0000        	call	c_ltor
1001  02fc ae0040        	ldw	x,#L231
1002  02ff cc0000        	jp	c_lcmp
1003  0302               LC021:
1004  0302 ae0000        	ldw	x,#_TimingDelay
1005  0305 cd0000        	call	c_ltor
1007  0308 ae0014        	ldw	x,#L45
1008  030b cc0000        	jp	c_lcmp
1009  030e               LC022:
1010  030e ae0000        	ldw	x,#_TimingDelay
1011  0311 cd0000        	call	c_ltor
1013  0314 ae0020        	ldw	x,#L27
1014  0317 cc0000        	jp	c_lcmp
1015  031a               LC023:
1016  031a ae0000        	ldw	x,#_TimingDelay
1017  031d cd0000        	call	c_ltor
1019  0320 ae0024        	ldw	x,#L67
1020  0323 cc0000        	jp	c_lcmp
1021  0326               LC024:
1022  0326 ae0000        	ldw	x,#_TimingDelay
1023  0329 cd0000        	call	c_ltor
1025  032c ae002c        	ldw	x,#L601
1026  032f cc0000        	jp	c_lcmp
1027  0332               LC025:
1028  0332 ae0000        	ldw	x,#_TimingDelay
1029  0335 cd0000        	call	c_ltor
1031  0338 ae0030        	ldw	x,#L211
1032  033b cc0000        	jp	c_lcmp
1033  033e               LC026:
1034  033e ae0000        	ldw	x,#_TimingDelay
1035  0341 cd0000        	call	c_ltor
1037  0344 ae0064        	ldw	x,#L052
1038  0347 81            	ret	
1080                     ; 366 void UpdateSwitch(void)
1080                     ; 367 {
1081                     .text:	section	.text,new
1082  0000               _UpdateSwitch:
1084  0000 88            	push	a
1085       00000001      OFST:	set	1
1088                     ; 369 	uint8_t val = 0;
1090                     ; 370 	if (input1 == 0)	
1092  0001 b60c          	ld	a,_input1
1093  0003 263f          	jrne	L124
1094                     ; 372 			In1_0++;
1096  0005 be06          	ldw	x,_In1_0
1097  0007 5c            	incw	x
1098  0008 bf06          	ldw	_In1_0,x
1099                     ; 373 			In1_1 = 0;
1101  000a 5f            	clrw	x
1102  000b bf08          	ldw	_In1_1,x
1103                     ; 374 			if ((In1_0 > Debounce)&& (StatoIn1 == 1))
1105  000d be06          	ldw	x,_In1_0
1106  000f b30a          	cpw	x,_Debounce
1107  0011 2346          	jrule	L724
1109  0013 b605          	ld	a,_StatoIn1
1110  0015 4a            	dec	a
1111  0016 2641          	jrne	L724
1112                     ; 376 				In1_0 = Debounce + 1;
1114  0018 be0a          	ldw	x,_Debounce
1115  001a 5c            	incw	x
1116  001b bf06          	ldw	_In1_0,x
1117                     ; 377 				StatoIn1 = 0;
1119  001d b705          	ld	_StatoIn1,a
1120                     ; 378 				flash_count++;
1122  001f 3c04          	inc	_flash_count
1123                     ; 380 				if (flash_count == 8) 
1125  0021 b604          	ld	a,_flash_count
1126  0023 a108          	cp	a,#8
1127  0025 2602          	jrne	L524
1128                     ; 382 					flash_count = 0;
1130  0027 3f04          	clr	_flash_count
1131  0029               L524:
1132                     ; 392 				FLASH_ProgramByte((0x40A5), flash_count);
1134  0029 3b0004        	push	_flash_count
1135  002c ae40a5        	ldw	x,#16549
1136  002f 89            	pushw	x
1137  0030 5f            	clrw	x
1138  0031 89            	pushw	x
1139  0032 cd0000        	call	_FLASH_ProgramByte
1141  0035 5b05          	addw	sp,#5
1142                     ; 393 				val = FLASH_ReadByte(0x40A5);
1144  0037 ae40a5        	ldw	x,#16549
1145  003a 89            	pushw	x
1146  003b 5f            	clrw	x
1147  003c 89            	pushw	x
1148  003d cd0000        	call	_FLASH_ReadByte
1150  0040 5b04          	addw	sp,#4
1151  0042 2015          	jra	L724
1152  0044               L124:
1153                     ; 398 			In1_0 = 0;
1155  0044 5f            	clrw	x
1156  0045 bf06          	ldw	_In1_0,x
1157                     ; 399 			In1_1++;
1159  0047 be08          	ldw	x,_In1_1
1160  0049 5c            	incw	x
1161  004a bf08          	ldw	_In1_1,x
1162                     ; 400 			if ((In1_1 > Debounce))
1164  004c b30a          	cpw	x,_Debounce
1165  004e 2309          	jrule	L724
1166                     ; 402 				In1_1 = Debounce + 1;
1168  0050 be0a          	ldw	x,_Debounce
1169  0052 5c            	incw	x
1170  0053 bf08          	ldw	_In1_1,x
1171                     ; 403 				StatoIn1 = 1;
1173  0055 35010005      	mov	_StatoIn1,#1
1174  0059               L724:
1175                     ; 407 }
1178  0059 84            	pop	a
1179  005a 81            	ret	
1203                     ; 413 static void CLK_Config(void)
1203                     ; 414 {
1204                     .text:	section	.text,new
1205  0000               L3_CLK_Config:
1209                     ; 417     CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
1211  0000 4f            	clr	a
1213                     ; 418 }
1216  0001 cc0000        	jp	_CLK_HSIPrescalerConfig
1244                     ; 425 static void TIM4_Config(void)
1244                     ; 426 {
1245                     .text:	section	.text,new
1246  0000               L5_TIM4_Config:
1250                     ; 437   TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
1252  0000 ae077c        	ldw	x,#1916
1253  0003 cd0000        	call	_TIM4_TimeBaseInit
1255                     ; 439   TIM4_ClearFlag(TIM4_FLAG_UPDATE);
1257  0006 a601          	ld	a,#1
1258  0008 cd0000        	call	_TIM4_ClearFlag
1260                     ; 441   TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
1262  000b ae0101        	ldw	x,#257
1263  000e cd0000        	call	_TIM4_ITConfig
1265                     ; 444   enableInterrupts();
1268  0011 9a            	rim	
1270                     ; 447   TIM4_Cmd(ENABLE);
1273  0012 a601          	ld	a,#1
1275                     ; 448 }
1278  0014 cc0000        	jp	_TIM4_Cmd
1302                     ; 455 static void GPIO_Config(void)
1302                     ; 456 {
1303                     .text:	section	.text,new
1304  0000               L7_GPIO_Config:
1308                     ; 457 		GPIO_Init(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST);
1310  0000 4be0          	push	#224
1311  0002 4b08          	push	#8
1312  0004 ae5000        	ldw	x,#20480
1313  0007 cd0000        	call	_GPIO_Init
1315  000a 85            	popw	x
1316                     ; 458 		GPIO_Init(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST);
1318  000b 4be0          	push	#224
1319  000d 4b08          	push	#8
1320  000f ae500a        	ldw	x,#20490
1321  0012 cd0000        	call	_GPIO_Init
1323  0015 85            	popw	x
1324                     ; 459 		GPIO_Init(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_1, GPIO_MODE_IN_PU_NO_IT);
1326  0016 4b40          	push	#64
1327  0018 4b02          	push	#2
1328  001a ae5000        	ldw	x,#20480
1329  001d cd0000        	call	_GPIO_Init
1331  0020 85            	popw	x
1332                     ; 460 }
1335  0021 81            	ret	
1370                     ; 467 void Delay(__IO uint32_t nTime)
1370                     ; 468 {
1371                     .text:	section	.text,new
1372  0000               _Delay:
1374       00000000      OFST:	set	0
1377                     ; 469   TimingDelay = nTime;
1379  0000 1e05          	ldw	x,(OFST+5,sp)
1380  0002 bf02          	ldw	_TimingDelay+2,x
1381  0004 1e03          	ldw	x,(OFST+3,sp)
1382  0006 bf00          	ldw	_TimingDelay,x
1383                     ; 472 }
1386  0008 81            	ret	
1413                     ; 479 void TimingDelay_Decrement(void)
1413                     ; 480 {
1414                     .text:	section	.text,new
1415  0000               _TimingDelay_Decrement:
1419                     ; 481   if (TimingDelay != 0)
1421  0000 ae0000        	ldw	x,#_TimingDelay
1422  0003 cd0000        	call	c_lzmp
1424  0006 2707          	jreq	L115
1425                     ; 483     TimingDelay--;
1427  0008 a601          	ld	a,#1
1428  000a cd0000        	call	c_lgsbc
1431  000d 2008          	jra	L315
1432  000f               L115:
1433                     ; 485 	else TimingDelay = TOTAL_PERIOD;
1435  000f ae0514        	ldw	x,#1300
1436  0012 bf02          	ldw	_TimingDelay+2,x
1437  0014 5f            	clrw	x
1438  0015 bf00          	ldw	_TimingDelay,x
1439  0017               L315:
1440                     ; 487 	input1 = GPIO_ReadInputPin(GPIOA, GPIO_PIN_1);
1442  0017 4b02          	push	#2
1443  0019 ae5000        	ldw	x,#20480
1444  001c cd0000        	call	_GPIO_ReadInputPin
1446  001f 5b01          	addw	sp,#1
1447  0021 b70c          	ld	_input1,a
1448                     ; 488 }
1451  0023 81            	ret	
1486                     ; 499 void assert_failed(uint8_t* file, uint32_t line)
1486                     ; 500 { 
1487                     .text:	section	.text,new
1488  0000               _assert_failed:
1492  0000               L335:
1493  0000 20fe          	jra	L335
1601                     	xdef	_main
1602                     	xdef	_UpdateSwitch
1603                     	xdef	_TimingDelay_Decrement
1604                     	xdef	_Delay
1605                     	switch	.ubsct
1606  0000               _OperationStatus:
1607  0000 00            	ds.b	1
1608                     	xdef	_OperationStatus
1609                     	xdef	_input1
1610                     	xdef	_Debounce
1611                     	xdef	_In1_1
1612                     	xdef	_In1_0
1613                     	xdef	_StatoIn1
1614                     	xdef	_flash_count
1615                     	xdef	_TimingDelay
1616                     	xdef	_assert_failed
1617                     	xref	_TIM4_ClearFlag
1618                     	xref	_TIM4_ITConfig
1619                     	xref	_TIM4_Cmd
1620                     	xref	_TIM4_TimeBaseInit
1621                     	xref	_GPIO_ReadInputPin
1622                     	xref	_GPIO_WriteLow
1623                     	xref	_GPIO_WriteHigh
1624                     	xref	_GPIO_Init
1625                     	xref	_FLASH_SetProgrammingTime
1626                     	xref	_FLASH_ReadByte
1627                     	xref	_FLASH_ProgramByte
1628                     	xref	_FLASH_Unlock
1629                     	xref	_CLK_HSIPrescalerConfig
1649                     	xref	c_lgsbc
1650                     	xref	c_lzmp
1651                     	xref	c_lcmp
1652                     	xref	c_ltor
1653                     	end
