   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.13 - 05 Feb 2019
   3                     ; Generator (Limited) V4.4.9 - 06 Feb 2019
   4                     ; Optimizer V4.4.9 - 06 Feb 2019
  50                     ; 54 void UART3_DeInit(void)
  50                     ; 55 {
  52                     .text:	section	.text,new
  53  0000               _UART3_DeInit:
  57                     ; 58   (void) UART3->SR;
  59  0000 c65240        	ld	a,21056
  60                     ; 59   (void) UART3->DR;
  62  0003 c65241        	ld	a,21057
  63                     ; 61   UART3->BRR2 = UART3_BRR2_RESET_VALUE; /*Set UART3_BRR2 to reset value 0x00 */
  65  0006 725f5243      	clr	21059
  66                     ; 62   UART3->BRR1 = UART3_BRR1_RESET_VALUE; /*Set UART3_BRR1 to reset value 0x00 */
  68  000a 725f5242      	clr	21058
  69                     ; 64   UART3->CR1 = UART3_CR1_RESET_VALUE;  /*Set UART3_CR1 to reset value 0x00  */
  71  000e 725f5244      	clr	21060
  72                     ; 65   UART3->CR2 = UART3_CR2_RESET_VALUE;  /*Set UART3_CR2 to reset value 0x00  */
  74  0012 725f5245      	clr	21061
  75                     ; 66   UART3->CR3 = UART3_CR3_RESET_VALUE;  /*Set UART3_CR3 to reset value 0x00  */
  77  0016 725f5246      	clr	21062
  78                     ; 67   UART3->CR4 = UART3_CR4_RESET_VALUE;  /*Set UART3_CR4 to reset value 0x00  */
  80  001a 725f5247      	clr	21063
  81                     ; 68   UART3->CR6 = UART3_CR6_RESET_VALUE;  /*Set UART3_CR6 to reset value 0x00  */
  83  001e 725f5249      	clr	21065
  84                     ; 69 }
  87  0022 81            	ret	
 305                     .const:	section	.text
 306  0000               L24:
 307  0000 00098969      	dc.l	625001
 308  0004               L46:
 309  0004 00000064      	dc.l	100
 310                     ; 83 void UART3_Init(uint32_t BaudRate, UART3_WordLength_TypeDef WordLength, 
 310                     ; 84                 UART3_StopBits_TypeDef StopBits, UART3_Parity_TypeDef Parity, 
 310                     ; 85                 UART3_Mode_TypeDef Mode)
 310                     ; 86 {
 311                     .text:	section	.text,new
 312  0000               _UART3_Init:
 314  0000 520e          	subw	sp,#14
 315       0000000e      OFST:	set	14
 318                     ; 87   uint8_t BRR2_1 = 0, BRR2_2 = 0;
 322                     ; 88   uint32_t BaudRate_Mantissa = 0, BaudRate_Mantissa100 = 0;
 326                     ; 91   assert_param(IS_UART3_WORDLENGTH_OK(WordLength));
 328  0002 7b15          	ld	a,(OFST+7,sp)
 329  0004 270a          	jreq	L41
 330  0006 a110          	cp	a,#16
 331  0008 2706          	jreq	L41
 332  000a ae005b        	ldw	x,#91
 333  000d cd0155        	call	LC001
 334  0010               L41:
 335                     ; 92   assert_param(IS_UART3_STOPBITS_OK(StopBits));
 337  0010 7b16          	ld	a,(OFST+8,sp)
 338  0012 270a          	jreq	L42
 339  0014 a120          	cp	a,#32
 340  0016 2706          	jreq	L42
 341  0018 ae005c        	ldw	x,#92
 342  001b cd0155        	call	LC001
 343  001e               L42:
 344                     ; 93   assert_param(IS_UART3_PARITY_OK(Parity));
 346  001e 7b17          	ld	a,(OFST+9,sp)
 347  0020 270e          	jreq	L43
 348  0022 a104          	cp	a,#4
 349  0024 270a          	jreq	L43
 350  0026 a106          	cp	a,#6
 351  0028 2706          	jreq	L43
 352  002a ae005d        	ldw	x,#93
 353  002d cd0155        	call	LC001
 354  0030               L43:
 355                     ; 94   assert_param(IS_UART3_BAUDRATE_OK(BaudRate));
 357  0030 96            	ldw	x,sp
 358  0031 1c0011        	addw	x,#OFST+3
 359  0034 cd0000        	call	c_ltor
 361  0037 ae0000        	ldw	x,#L24
 362  003a cd0000        	call	c_lcmp
 364  003d 2506          	jrult	L44
 365  003f ae005e        	ldw	x,#94
 366  0042 cd0155        	call	LC001
 367  0045               L44:
 368                     ; 95   assert_param(IS_UART3_MODE_OK((uint8_t)Mode));
 370  0045 7b18          	ld	a,(OFST+10,sp)
 371  0047 a108          	cp	a,#8
 372  0049 2722          	jreq	L45
 373  004b a140          	cp	a,#64
 374  004d 271e          	jreq	L45
 375  004f a104          	cp	a,#4
 376  0051 271a          	jreq	L45
 377  0053 a180          	cp	a,#128
 378  0055 2716          	jreq	L45
 379  0057 a10c          	cp	a,#12
 380  0059 2712          	jreq	L45
 381  005b a144          	cp	a,#68
 382  005d 270e          	jreq	L45
 383  005f a1c0          	cp	a,#192
 384  0061 270a          	jreq	L45
 385  0063 a188          	cp	a,#136
 386  0065 2706          	jreq	L45
 387  0067 ae005f        	ldw	x,#95
 388  006a cd0155        	call	LC001
 389  006d               L45:
 390                     ; 98   UART3->CR1 &= (uint8_t)(~UART3_CR1_M);     
 392  006d 72195244      	bres	21060,#4
 393                     ; 100   UART3->CR1 |= (uint8_t)WordLength; 
 395  0071 c65244        	ld	a,21060
 396  0074 1a15          	or	a,(OFST+7,sp)
 397  0076 c75244        	ld	21060,a
 398                     ; 103   UART3->CR3 &= (uint8_t)(~UART3_CR3_STOP);  
 400  0079 c65246        	ld	a,21062
 401  007c a4cf          	and	a,#207
 402  007e c75246        	ld	21062,a
 403                     ; 105   UART3->CR3 |= (uint8_t)StopBits;  
 405  0081 c65246        	ld	a,21062
 406  0084 1a16          	or	a,(OFST+8,sp)
 407  0086 c75246        	ld	21062,a
 408                     ; 108   UART3->CR1 &= (uint8_t)(~(UART3_CR1_PCEN | UART3_CR1_PS));  
 410  0089 c65244        	ld	a,21060
 411  008c a4f9          	and	a,#249
 412  008e c75244        	ld	21060,a
 413                     ; 110   UART3->CR1 |= (uint8_t)Parity;     
 415  0091 c65244        	ld	a,21060
 416  0094 1a17          	or	a,(OFST+9,sp)
 417  0096 c75244        	ld	21060,a
 418                     ; 113   UART3->BRR1 &= (uint8_t)(~UART3_BRR1_DIVM);  
 420  0099 725f5242      	clr	21058
 421                     ; 115   UART3->BRR2 &= (uint8_t)(~UART3_BRR2_DIVM);  
 423  009d c65243        	ld	a,21059
 424  00a0 a40f          	and	a,#15
 425  00a2 c75243        	ld	21059,a
 426                     ; 117   UART3->BRR2 &= (uint8_t)(~UART3_BRR2_DIVF);  
 428  00a5 c65243        	ld	a,21059
 429  00a8 a4f0          	and	a,#240
 430  00aa c75243        	ld	21059,a
 431                     ; 120   BaudRate_Mantissa    = ((uint32_t)CLK_GetClockFreq() / (BaudRate << 4));
 433  00ad 96            	ldw	x,sp
 434  00ae cd0161        	call	LC002
 436  00b1 96            	ldw	x,sp
 437  00b2 5c            	incw	x
 438  00b3 cd0000        	call	c_rtol
 441  00b6 cd0000        	call	_CLK_GetClockFreq
 443  00b9 96            	ldw	x,sp
 444  00ba 5c            	incw	x
 445  00bb cd0000        	call	c_ludv
 447  00be 96            	ldw	x,sp
 448  00bf 1c000b        	addw	x,#OFST-3
 449  00c2 cd0000        	call	c_rtol
 452                     ; 121   BaudRate_Mantissa100 = (((uint32_t)CLK_GetClockFreq() * 100) / (BaudRate << 4));
 454  00c5 96            	ldw	x,sp
 455  00c6 cd0161        	call	LC002
 457  00c9 96            	ldw	x,sp
 458  00ca 5c            	incw	x
 459  00cb cd0000        	call	c_rtol
 462  00ce cd0000        	call	_CLK_GetClockFreq
 464  00d1 a664          	ld	a,#100
 465  00d3 cd0000        	call	c_smul
 467  00d6 96            	ldw	x,sp
 468  00d7 5c            	incw	x
 469  00d8 cd0000        	call	c_ludv
 471  00db 96            	ldw	x,sp
 472  00dc 1c0007        	addw	x,#OFST-7
 473  00df cd0000        	call	c_rtol
 476                     ; 124   BRR2_1 = (uint8_t)((uint8_t)(((BaudRate_Mantissa100 - (BaudRate_Mantissa * 100))
 476                     ; 125                                 << 4) / 100) & (uint8_t)0x0F); 
 478  00e2 96            	ldw	x,sp
 479  00e3 1c000b        	addw	x,#OFST-3
 480  00e6 cd0000        	call	c_ltor
 482  00e9 a664          	ld	a,#100
 483  00eb cd0000        	call	c_smul
 485  00ee 96            	ldw	x,sp
 486  00ef 5c            	incw	x
 487  00f0 cd0000        	call	c_rtol
 490  00f3 96            	ldw	x,sp
 491  00f4 1c0007        	addw	x,#OFST-7
 492  00f7 cd0000        	call	c_ltor
 494  00fa 96            	ldw	x,sp
 495  00fb 5c            	incw	x
 496  00fc cd0000        	call	c_lsub
 498  00ff a604          	ld	a,#4
 499  0101 cd0000        	call	c_llsh
 501  0104 ae0004        	ldw	x,#L46
 502  0107 cd0000        	call	c_ludv
 504  010a b603          	ld	a,c_lreg+3
 505  010c a40f          	and	a,#15
 506  010e 6b05          	ld	(OFST-9,sp),a
 508                     ; 126   BRR2_2 = (uint8_t)((BaudRate_Mantissa >> 4) & (uint8_t)0xF0);
 510  0110 96            	ldw	x,sp
 511  0111 1c000b        	addw	x,#OFST-3
 512  0114 cd0000        	call	c_ltor
 514  0117 a604          	ld	a,#4
 515  0119 cd0000        	call	c_lursh
 517  011c b603          	ld	a,c_lreg+3
 518  011e a4f0          	and	a,#240
 519  0120 b703          	ld	c_lreg+3,a
 520  0122 3f02          	clr	c_lreg+2
 521  0124 3f01          	clr	c_lreg+1
 522  0126 3f00          	clr	c_lreg
 523  0128 6b06          	ld	(OFST-8,sp),a
 525                     ; 128   UART3->BRR2 = (uint8_t)(BRR2_1 | BRR2_2);
 527  012a 1a05          	or	a,(OFST-9,sp)
 528  012c c75243        	ld	21059,a
 529                     ; 130   UART3->BRR1 = (uint8_t)BaudRate_Mantissa;           
 531  012f 7b0e          	ld	a,(OFST+0,sp)
 532  0131 c75242        	ld	21058,a
 533                     ; 132   if ((uint8_t)(Mode & UART3_MODE_TX_ENABLE))
 535  0134 7b18          	ld	a,(OFST+10,sp)
 536  0136 a504          	bcp	a,#4
 537  0138 2706          	jreq	L731
 538                     ; 135     UART3->CR2 |= UART3_CR2_TEN;  
 540  013a 72165245      	bset	21061,#3
 542  013e 2004          	jra	L141
 543  0140               L731:
 544                     ; 140     UART3->CR2 &= (uint8_t)(~UART3_CR2_TEN);  
 546  0140 72175245      	bres	21061,#3
 547  0144               L141:
 548                     ; 142   if ((uint8_t)(Mode & UART3_MODE_RX_ENABLE))
 550  0144 a508          	bcp	a,#8
 551  0146 2706          	jreq	L341
 552                     ; 145     UART3->CR2 |= UART3_CR2_REN;  
 554  0148 72145245      	bset	21061,#2
 556  014c 2004          	jra	L541
 557  014e               L341:
 558                     ; 150     UART3->CR2 &= (uint8_t)(~UART3_CR2_REN);  
 560  014e 72155245      	bres	21061,#2
 561  0152               L541:
 562                     ; 152 }
 565  0152 5b0e          	addw	sp,#14
 566  0154 81            	ret	
 567  0155               LC001:
 568  0155 89            	pushw	x
 569  0156 5f            	clrw	x
 570  0157 89            	pushw	x
 571  0158 ae0008        	ldw	x,#L531
 572  015b cd0000        	call	_assert_failed
 574  015e 5b04          	addw	sp,#4
 575  0160 81            	ret	
 576  0161               LC002:
 577  0161 1c0011        	addw	x,#OFST+3
 578  0164 cd0000        	call	c_ltor
 580  0167 a604          	ld	a,#4
 581  0169 cc0000        	jp	c_llsh
 636                     ; 160 void UART3_Cmd(FunctionalState NewState)
 636                     ; 161 {
 637                     .text:	section	.text,new
 638  0000               _UART3_Cmd:
 642                     ; 162   if (NewState != DISABLE)
 644  0000 4d            	tnz	a
 645  0001 2705          	jreq	L571
 646                     ; 165     UART3->CR1 &= (uint8_t)(~UART3_CR1_UARTD); 
 648  0003 721b5244      	bres	21060,#5
 651  0007 81            	ret	
 652  0008               L571:
 653                     ; 170     UART3->CR1 |= UART3_CR1_UARTD;  
 655  0008 721a5244      	bset	21060,#5
 656                     ; 172 }
 659  000c 81            	ret	
 788                     ; 189 void UART3_ITConfig(UART3_IT_TypeDef UART3_IT, FunctionalState NewState)
 788                     ; 190 {
 789                     .text:	section	.text,new
 790  0000               _UART3_ITConfig:
 792  0000 89            	pushw	x
 793  0001 89            	pushw	x
 794       00000002      OFST:	set	2
 797                     ; 191   uint8_t uartreg = 0, itpos = 0x00;
 801                     ; 194   assert_param(IS_UART3_CONFIG_IT_OK(UART3_IT));
 803  0002 a30100        	cpw	x,#256
 804  0005 2724          	jreq	L67
 805  0007 a30277        	cpw	x,#631
 806  000a 271f          	jreq	L67
 807  000c a30266        	cpw	x,#614
 808  000f 271a          	jreq	L67
 809  0011 a30205        	cpw	x,#517
 810  0014 2715          	jreq	L67
 811  0016 a30244        	cpw	x,#580
 812  0019 2710          	jreq	L67
 813  001b a30412        	cpw	x,#1042
 814  001e 270b          	jreq	L67
 815  0020 a30346        	cpw	x,#838
 816  0023 2706          	jreq	L67
 817  0025 ae00c2        	ldw	x,#194
 818  0028 cd00b5        	call	LC007
 819  002b               L67:
 820                     ; 195   assert_param(IS_FUNCTIONALSTATE_OK(NewState));
 822  002b 7b07          	ld	a,(OFST+5,sp)
 823  002d 2708          	jreq	L601
 824  002f 4a            	dec	a
 825  0030 2705          	jreq	L601
 826  0032 ae00c3        	ldw	x,#195
 827  0035 ad7e          	call	LC007
 828  0037               L601:
 829                     ; 198   uartreg = (uint8_t)((uint16_t)UART3_IT >> 0x08);
 831  0037 7b03          	ld	a,(OFST+1,sp)
 832  0039 6b01          	ld	(OFST-1,sp),a
 834                     ; 201   itpos = (uint8_t)((uint8_t)1 << (uint8_t)((uint8_t)UART3_IT & (uint8_t)0x0F));
 836  003b 7b04          	ld	a,(OFST+2,sp)
 837  003d a40f          	and	a,#15
 838  003f 5f            	clrw	x
 839  0040 97            	ld	xl,a
 840  0041 a601          	ld	a,#1
 841  0043 5d            	tnzw	x
 842  0044 2704          	jreq	L211
 843  0046               L411:
 844  0046 48            	sll	a
 845  0047 5a            	decw	x
 846  0048 26fc          	jrne	L411
 847  004a               L211:
 848  004a 6b02          	ld	(OFST+0,sp),a
 850                     ; 203   if (NewState != DISABLE)
 852  004c 7b07          	ld	a,(OFST+5,sp)
 853  004e 272a          	jreq	L552
 854                     ; 206     if (uartreg == 0x01)
 856  0050 7b01          	ld	a,(OFST-1,sp)
 857  0052 a101          	cp	a,#1
 858  0054 2607          	jrne	L752
 859                     ; 208       UART3->CR1 |= itpos;
 861  0056 c65244        	ld	a,21060
 862  0059 1a02          	or	a,(OFST+0,sp)
 864  005b 2029          	jp	LC005
 865  005d               L752:
 866                     ; 210     else if (uartreg == 0x02)
 868  005d a102          	cp	a,#2
 869  005f 2607          	jrne	L362
 870                     ; 212       UART3->CR2 |= itpos;
 872  0061 c65245        	ld	a,21061
 873  0064 1a02          	or	a,(OFST+0,sp)
 875  0066 202d          	jp	LC004
 876  0068               L362:
 877                     ; 214     else if (uartreg == 0x03)
 879  0068 a103          	cp	a,#3
 880  006a 2607          	jrne	L762
 881                     ; 216       UART3->CR4 |= itpos;
 883  006c c65247        	ld	a,21063
 884  006f 1a02          	or	a,(OFST+0,sp)
 886  0071 2031          	jp	LC006
 887  0073               L762:
 888                     ; 220       UART3->CR6 |= itpos;
 890  0073 c65249        	ld	a,21065
 891  0076 1a02          	or	a,(OFST+0,sp)
 892  0078 2035          	jp	LC003
 893  007a               L552:
 894                     ; 226     if (uartreg == 0x01)
 896  007a 7b01          	ld	a,(OFST-1,sp)
 897  007c a101          	cp	a,#1
 898  007e 260b          	jrne	L572
 899                     ; 228       UART3->CR1 &= (uint8_t)(~itpos);
 901  0080 7b02          	ld	a,(OFST+0,sp)
 902  0082 43            	cpl	a
 903  0083 c45244        	and	a,21060
 904  0086               LC005:
 905  0086 c75244        	ld	21060,a
 907  0089 2027          	jra	L372
 908  008b               L572:
 909                     ; 230     else if (uartreg == 0x02)
 911  008b a102          	cp	a,#2
 912  008d 260b          	jrne	L103
 913                     ; 232       UART3->CR2 &= (uint8_t)(~itpos);
 915  008f 7b02          	ld	a,(OFST+0,sp)
 916  0091 43            	cpl	a
 917  0092 c45245        	and	a,21061
 918  0095               LC004:
 919  0095 c75245        	ld	21061,a
 921  0098 2018          	jra	L372
 922  009a               L103:
 923                     ; 234     else if (uartreg == 0x03)
 925  009a a103          	cp	a,#3
 926  009c 260b          	jrne	L503
 927                     ; 236       UART3->CR4 &= (uint8_t)(~itpos);
 929  009e 7b02          	ld	a,(OFST+0,sp)
 930  00a0 43            	cpl	a
 931  00a1 c45247        	and	a,21063
 932  00a4               LC006:
 933  00a4 c75247        	ld	21063,a
 935  00a7 2009          	jra	L372
 936  00a9               L503:
 937                     ; 240       UART3->CR6 &= (uint8_t)(~itpos);
 939  00a9 7b02          	ld	a,(OFST+0,sp)
 940  00ab 43            	cpl	a
 941  00ac c45249        	and	a,21065
 942  00af               LC003:
 943  00af c75249        	ld	21065,a
 944  00b2               L372:
 945                     ; 243 }
 948  00b2 5b04          	addw	sp,#4
 949  00b4 81            	ret	
 950  00b5               LC007:
 951  00b5 89            	pushw	x
 952  00b6 5f            	clrw	x
 953  00b7 89            	pushw	x
 954  00b8 ae0008        	ldw	x,#L531
 955  00bb cd0000        	call	_assert_failed
 957  00be 5b04          	addw	sp,#4
 958  00c0 81            	ret	
1018                     ; 252 void UART3_LINBreakDetectionConfig(UART3_LINBreakDetectionLength_TypeDef UART3_LINBreakDetectionLength)
1018                     ; 253 {
1019                     .text:	section	.text,new
1020  0000               _UART3_LINBreakDetectionConfig:
1022  0000 88            	push	a
1023       00000000      OFST:	set	0
1026                     ; 255   assert_param(IS_UART3_LINBREAKDETECTIONLENGTH_OK(UART3_LINBreakDetectionLength));
1028  0001 4d            	tnz	a
1029  0002 2711          	jreq	L421
1030  0004 4a            	dec	a
1031  0005 270e          	jreq	L421
1032  0007 ae00ff        	ldw	x,#255
1033  000a 89            	pushw	x
1034  000b 5f            	clrw	x
1035  000c 89            	pushw	x
1036  000d ae0008        	ldw	x,#L531
1037  0010 cd0000        	call	_assert_failed
1039  0013 5b04          	addw	sp,#4
1040  0015               L421:
1041                     ; 257   if (UART3_LINBreakDetectionLength != UART3_LINBREAKDETECTIONLENGTH_10BITS)
1043  0015 7b01          	ld	a,(OFST+1,sp)
1044  0017 2706          	jreq	L733
1045                     ; 259     UART3->CR4 |= UART3_CR4_LBDL;
1047  0019 721a5247      	bset	21063,#5
1049  001d 2004          	jra	L143
1050  001f               L733:
1051                     ; 263     UART3->CR4 &= ((uint8_t)~UART3_CR4_LBDL);
1053  001f 721b5247      	bres	21063,#5
1054  0023               L143:
1055                     ; 265 }
1058  0023 84            	pop	a
1059  0024 81            	ret	
1181                     ; 277 void UART3_LINConfig(UART3_LinMode_TypeDef UART3_Mode,
1181                     ; 278                      UART3_LinAutosync_TypeDef UART3_Autosync, 
1181                     ; 279                      UART3_LinDivUp_TypeDef UART3_DivUp)
1181                     ; 280 {
1182                     .text:	section	.text,new
1183  0000               _UART3_LINConfig:
1185  0000 89            	pushw	x
1186       00000000      OFST:	set	0
1189                     ; 282   assert_param(IS_UART3_SLAVE_OK(UART3_Mode));
1191  0001 9e            	ld	a,xh
1192  0002 4d            	tnz	a
1193  0003 2709          	jreq	L631
1194  0005 9e            	ld	a,xh
1195  0006 4a            	dec	a
1196  0007 2705          	jreq	L631
1197  0009 ae011a        	ldw	x,#282
1198  000c ad46          	call	LC008
1199  000e               L631:
1200                     ; 283   assert_param(IS_UART3_AUTOSYNC_OK(UART3_Autosync));
1202  000e 7b02          	ld	a,(OFST+2,sp)
1203  0010 4a            	dec	a
1204  0011 2709          	jreq	L641
1205  0013 7b02          	ld	a,(OFST+2,sp)
1206  0015 2705          	jreq	L641
1207  0017 ae011b        	ldw	x,#283
1208  001a ad38          	call	LC008
1209  001c               L641:
1210                     ; 284   assert_param(IS_UART3_DIVUP_OK(UART3_DivUp));
1212  001c 7b05          	ld	a,(OFST+5,sp)
1213  001e 2708          	jreq	L651
1214  0020 4a            	dec	a
1215  0021 2705          	jreq	L651
1216  0023 ae011c        	ldw	x,#284
1217  0026 ad2c          	call	LC008
1218  0028               L651:
1219                     ; 286   if (UART3_Mode != UART3_LIN_MODE_MASTER)
1221  0028 7b01          	ld	a,(OFST+1,sp)
1222  002a 2706          	jreq	L124
1223                     ; 288     UART3->CR6 |=  UART3_CR6_LSLV;
1225  002c 721a5249      	bset	21065,#5
1227  0030 2004          	jra	L324
1228  0032               L124:
1229                     ; 292     UART3->CR6 &= ((uint8_t)~UART3_CR6_LSLV);
1231  0032 721b5249      	bres	21065,#5
1232  0036               L324:
1233                     ; 295   if (UART3_Autosync != UART3_LIN_AUTOSYNC_DISABLE)
1235  0036 7b02          	ld	a,(OFST+2,sp)
1236  0038 2706          	jreq	L524
1237                     ; 297     UART3->CR6 |=  UART3_CR6_LASE ;
1239  003a 72185249      	bset	21065,#4
1241  003e 2004          	jra	L724
1242  0040               L524:
1243                     ; 301     UART3->CR6 &= ((uint8_t)~ UART3_CR6_LASE );
1245  0040 72195249      	bres	21065,#4
1246  0044               L724:
1247                     ; 304   if (UART3_DivUp != UART3_LIN_DIVUP_LBRR1)
1249  0044 7b05          	ld	a,(OFST+5,sp)
1250  0046 2706          	jreq	L134
1251                     ; 306     UART3->CR6 |=  UART3_CR6_LDUM;
1253  0048 721e5249      	bset	21065,#7
1255  004c 2004          	jra	L334
1256  004e               L134:
1257                     ; 310     UART3->CR6 &= ((uint8_t)~ UART3_CR6_LDUM);
1259  004e 721f5249      	bres	21065,#7
1260  0052               L334:
1261                     ; 312 }
1264  0052 85            	popw	x
1265  0053 81            	ret	
1266  0054               LC008:
1267  0054 89            	pushw	x
1268  0055 5f            	clrw	x
1269  0056 89            	pushw	x
1270  0057 ae0008        	ldw	x,#L531
1271  005a cd0000        	call	_assert_failed
1273  005d 5b04          	addw	sp,#4
1274  005f 81            	ret	
1310                     ; 320 void UART3_LINCmd(FunctionalState NewState)
1310                     ; 321 {
1311                     .text:	section	.text,new
1312  0000               _UART3_LINCmd:
1314  0000 88            	push	a
1315       00000000      OFST:	set	0
1318                     ; 323   assert_param(IS_FUNCTIONALSTATE_OK(NewState));
1320  0001 4d            	tnz	a
1321  0002 2711          	jreq	L071
1322  0004 4a            	dec	a
1323  0005 270e          	jreq	L071
1324  0007 ae0143        	ldw	x,#323
1325  000a 89            	pushw	x
1326  000b 5f            	clrw	x
1327  000c 89            	pushw	x
1328  000d ae0008        	ldw	x,#L531
1329  0010 cd0000        	call	_assert_failed
1331  0013 5b04          	addw	sp,#4
1332  0015               L071:
1333                     ; 325   if (NewState != DISABLE)
1335  0015 7b01          	ld	a,(OFST+1,sp)
1336  0017 2706          	jreq	L354
1337                     ; 328     UART3->CR3 |= UART3_CR3_LINEN;
1339  0019 721c5246      	bset	21062,#6
1341  001d 2004          	jra	L554
1342  001f               L354:
1343                     ; 333     UART3->CR3 &= ((uint8_t)~UART3_CR3_LINEN);
1345  001f 721d5246      	bres	21062,#6
1346  0023               L554:
1347                     ; 335 }
1350  0023 84            	pop	a
1351  0024 81            	ret	
1409                     ; 343 void UART3_WakeUpConfig(UART3_WakeUp_TypeDef UART3_WakeUp)
1409                     ; 344 {
1410                     .text:	section	.text,new
1411  0000               _UART3_WakeUpConfig:
1413  0000 88            	push	a
1414       00000000      OFST:	set	0
1417                     ; 346   assert_param(IS_UART3_WAKEUP_OK(UART3_WakeUp));
1419  0001 4d            	tnz	a
1420  0002 2712          	jreq	L202
1421  0004 a108          	cp	a,#8
1422  0006 270e          	jreq	L202
1423  0008 ae015a        	ldw	x,#346
1424  000b 89            	pushw	x
1425  000c 5f            	clrw	x
1426  000d 89            	pushw	x
1427  000e ae0008        	ldw	x,#L531
1428  0011 cd0000        	call	_assert_failed
1430  0014 5b04          	addw	sp,#4
1431  0016               L202:
1432                     ; 348   UART3->CR1 &= ((uint8_t)~UART3_CR1_WAKE);
1434  0016 72175244      	bres	21060,#3
1435                     ; 349   UART3->CR1 |= (uint8_t)UART3_WakeUp;
1437  001a c65244        	ld	a,21060
1438  001d 1a01          	or	a,(OFST+1,sp)
1439  001f c75244        	ld	21060,a
1440                     ; 350 }
1443  0022 84            	pop	a
1444  0023 81            	ret	
1481                     ; 358 void UART3_ReceiverWakeUpCmd(FunctionalState NewState)
1481                     ; 359 {
1482                     .text:	section	.text,new
1483  0000               _UART3_ReceiverWakeUpCmd:
1485  0000 88            	push	a
1486       00000000      OFST:	set	0
1489                     ; 361   assert_param(IS_FUNCTIONALSTATE_OK(NewState));
1491  0001 4d            	tnz	a
1492  0002 2711          	jreq	L412
1493  0004 4a            	dec	a
1494  0005 270e          	jreq	L412
1495  0007 ae0169        	ldw	x,#361
1496  000a 89            	pushw	x
1497  000b 5f            	clrw	x
1498  000c 89            	pushw	x
1499  000d ae0008        	ldw	x,#L531
1500  0010 cd0000        	call	_assert_failed
1502  0013 5b04          	addw	sp,#4
1503  0015               L412:
1504                     ; 363   if (NewState != DISABLE)
1506  0015 7b01          	ld	a,(OFST+1,sp)
1507  0017 2706          	jreq	L325
1508                     ; 366     UART3->CR2 |= UART3_CR2_RWU;
1510  0019 72125245      	bset	21061,#1
1512  001d 2004          	jra	L525
1513  001f               L325:
1514                     ; 371     UART3->CR2 &= ((uint8_t)~UART3_CR2_RWU);
1516  001f 72135245      	bres	21061,#1
1517  0023               L525:
1518                     ; 373 }
1521  0023 84            	pop	a
1522  0024 81            	ret	
1545                     ; 380 uint8_t UART3_ReceiveData8(void)
1545                     ; 381 {
1546                     .text:	section	.text,new
1547  0000               _UART3_ReceiveData8:
1551                     ; 382   return ((uint8_t)UART3->DR);
1553  0000 c65241        	ld	a,21057
1556  0003 81            	ret	
1588                     ; 390 uint16_t UART3_ReceiveData9(void)
1588                     ; 391 {
1589                     .text:	section	.text,new
1590  0000               _UART3_ReceiveData9:
1592  0000 89            	pushw	x
1593       00000002      OFST:	set	2
1596                     ; 392   uint16_t temp = 0;
1598                     ; 394   temp = (uint16_t)(((uint16_t)((uint16_t)UART3->CR1 & (uint16_t)UART3_CR1_R8)) << 1);
1600  0001 c65244        	ld	a,21060
1601  0004 a480          	and	a,#128
1602  0006 5f            	clrw	x
1603  0007 02            	rlwa	x,a
1604  0008 58            	sllw	x
1605  0009 1f01          	ldw	(OFST-1,sp),x
1607                     ; 395   return (uint16_t)((((uint16_t)UART3->DR) | temp) & ((uint16_t)0x01FF));
1609  000b c65241        	ld	a,21057
1610  000e 5f            	clrw	x
1611  000f 97            	ld	xl,a
1612  0010 01            	rrwa	x,a
1613  0011 1a02          	or	a,(OFST+0,sp)
1614  0013 01            	rrwa	x,a
1615  0014 1a01          	or	a,(OFST-1,sp)
1616  0016 a401          	and	a,#1
1617  0018 01            	rrwa	x,a
1620  0019 5b02          	addw	sp,#2
1621  001b 81            	ret	
1653                     ; 403 void UART3_SendData8(uint8_t Data)
1653                     ; 404 {
1654                     .text:	section	.text,new
1655  0000               _UART3_SendData8:
1659                     ; 406   UART3->DR = Data;
1661  0000 c75241        	ld	21057,a
1662                     ; 407 }
1665  0003 81            	ret	
1697                     ; 414 void UART3_SendData9(uint16_t Data)
1697                     ; 415 {
1698                     .text:	section	.text,new
1699  0000               _UART3_SendData9:
1701  0000 89            	pushw	x
1702       00000000      OFST:	set	0
1705                     ; 417   UART3->CR1 &= ((uint8_t)~UART3_CR1_T8);                  
1707  0001 721d5244      	bres	21060,#6
1708                     ; 420   UART3->CR1 |= (uint8_t)(((uint8_t)(Data >> 2)) & UART3_CR1_T8); 
1710  0005 54            	srlw	x
1711  0006 54            	srlw	x
1712  0007 9f            	ld	a,xl
1713  0008 a440          	and	a,#64
1714  000a ca5244        	or	a,21060
1715  000d c75244        	ld	21060,a
1716                     ; 423   UART3->DR   = (uint8_t)(Data);                    
1718  0010 7b02          	ld	a,(OFST+2,sp)
1719  0012 c75241        	ld	21057,a
1720                     ; 424 }
1723  0015 85            	popw	x
1724  0016 81            	ret	
1747                     ; 431 void UART3_SendBreak(void)
1747                     ; 432 {
1748                     .text:	section	.text,new
1749  0000               _UART3_SendBreak:
1753                     ; 433   UART3->CR2 |= UART3_CR2_SBK;
1755  0000 72105245      	bset	21061,#0
1756                     ; 434 }
1759  0004 81            	ret	
1792                     ; 441 void UART3_SetAddress(uint8_t UART3_Address)
1792                     ; 442 {
1793                     .text:	section	.text,new
1794  0000               _UART3_SetAddress:
1796  0000 88            	push	a
1797       00000000      OFST:	set	0
1800                     ; 444   assert_param(IS_UART3_ADDRESS_OK(UART3_Address));
1802  0001 a110          	cp	a,#16
1803  0003 250e          	jrult	L632
1804  0005 ae01bc        	ldw	x,#444
1805  0008 89            	pushw	x
1806  0009 5f            	clrw	x
1807  000a 89            	pushw	x
1808  000b ae0008        	ldw	x,#L531
1809  000e cd0000        	call	_assert_failed
1811  0011 5b04          	addw	sp,#4
1812  0013               L632:
1813                     ; 447   UART3->CR4 &= ((uint8_t)~UART3_CR4_ADD);
1815  0013 c65247        	ld	a,21063
1816  0016 a4f0          	and	a,#240
1817  0018 c75247        	ld	21063,a
1818                     ; 449   UART3->CR4 |= UART3_Address;
1820  001b c65247        	ld	a,21063
1821  001e 1a01          	or	a,(OFST+1,sp)
1822  0020 c75247        	ld	21063,a
1823                     ; 450 }
1826  0023 84            	pop	a
1827  0024 81            	ret	
1985                     ; 458 FlagStatus UART3_GetFlagStatus(UART3_Flag_TypeDef UART3_FLAG)
1985                     ; 459 {
1986                     .text:	section	.text,new
1987  0000               _UART3_GetFlagStatus:
1989  0000 89            	pushw	x
1990  0001 88            	push	a
1991       00000001      OFST:	set	1
1994                     ; 460   FlagStatus status = RESET;
1996                     ; 463   assert_param(IS_UART3_FLAG_OK(UART3_FLAG));
1998  0002 a30080        	cpw	x,#128
1999  0005 2745          	jreq	L052
2000  0007 a30040        	cpw	x,#64
2001  000a 2740          	jreq	L052
2002  000c a30020        	cpw	x,#32
2003  000f 273b          	jreq	L052
2004  0011 a30010        	cpw	x,#16
2005  0014 2736          	jreq	L052
2006  0016 a30008        	cpw	x,#8
2007  0019 2731          	jreq	L052
2008  001b a30004        	cpw	x,#4
2009  001e 272c          	jreq	L052
2010  0020 a30002        	cpw	x,#2
2011  0023 2727          	jreq	L052
2012  0025 a30001        	cpw	x,#1
2013  0028 2722          	jreq	L052
2014  002a a30101        	cpw	x,#257
2015  002d 271d          	jreq	L052
2016  002f a30301        	cpw	x,#769
2017  0032 2718          	jreq	L052
2018  0034 a30302        	cpw	x,#770
2019  0037 2713          	jreq	L052
2020  0039 a30210        	cpw	x,#528
2021  003c 270e          	jreq	L052
2022  003e ae01cf        	ldw	x,#463
2023  0041 89            	pushw	x
2024  0042 5f            	clrw	x
2025  0043 89            	pushw	x
2026  0044 ae0008        	ldw	x,#L531
2027  0047 cd0000        	call	_assert_failed
2029  004a 5b04          	addw	sp,#4
2030  004c               L052:
2031                     ; 466   if (UART3_FLAG == UART3_FLAG_LBDF)
2033  004c 1e02          	ldw	x,(OFST+1,sp)
2034  004e a30210        	cpw	x,#528
2035  0051 2609          	jrne	L517
2036                     ; 468     if ((UART3->CR4 & (uint8_t)UART3_FLAG) != (uint8_t)0x00)
2038  0053 c65247        	ld	a,21063
2039  0056 1503          	bcp	a,(OFST+2,sp)
2040  0058 2725          	jreq	L147
2041                     ; 471       status = SET;
2043  005a 201f          	jp	LC010
2044                     ; 476       status = RESET;
2045  005c               L517:
2046                     ; 479   else if (UART3_FLAG == UART3_FLAG_SBK)
2048  005c a30101        	cpw	x,#257
2049  005f 2609          	jrne	L527
2050                     ; 481     if ((UART3->CR2 & (uint8_t)UART3_FLAG) != (uint8_t)0x00)
2052  0061 c65245        	ld	a,21061
2053  0064 1503          	bcp	a,(OFST+2,sp)
2054  0066 2717          	jreq	L147
2055                     ; 484       status = SET;
2057  0068 2011          	jp	LC010
2058                     ; 489       status = RESET;
2059  006a               L527:
2060                     ; 492   else if ((UART3_FLAG == UART3_FLAG_LHDF) || (UART3_FLAG == UART3_FLAG_LSF))
2062  006a a30302        	cpw	x,#770
2063  006d 2705          	jreq	L737
2065  006f a30301        	cpw	x,#769
2066  0072 260f          	jrne	L537
2067  0074               L737:
2068                     ; 494     if ((UART3->CR6 & (uint8_t)UART3_FLAG) != (uint8_t)0x00)
2070  0074 c65249        	ld	a,21065
2071  0077 1503          	bcp	a,(OFST+2,sp)
2072  0079 2704          	jreq	L147
2073                     ; 497       status = SET;
2075  007b               LC010:
2079  007b a601          	ld	a,#1
2082  007d 2001          	jra	L327
2083  007f               L147:
2084                     ; 502       status = RESET;
2089  007f 4f            	clr	a
2091  0080               L327:
2092                     ; 520   return  status;
2096  0080 5b03          	addw	sp,#3
2097  0082 81            	ret	
2098  0083               L537:
2099                     ; 507     if ((UART3->SR & (uint8_t)UART3_FLAG) != (uint8_t)0x00)
2101  0083 c65240        	ld	a,21056
2102  0086 1503          	bcp	a,(OFST+2,sp)
2103  0088 27f5          	jreq	L147
2104                     ; 510       status = SET;
2106  008a 20ef          	jp	LC010
2107                     ; 515       status = RESET;
2143                     ; 551 void UART3_ClearFlag(UART3_Flag_TypeDef UART3_FLAG)
2143                     ; 552 {
2144                     .text:	section	.text,new
2145  0000               _UART3_ClearFlag:
2147  0000 89            	pushw	x
2148       00000000      OFST:	set	0
2151                     ; 554   assert_param(IS_UART3_CLEAR_FLAG_OK(UART3_FLAG));
2153  0001 a30020        	cpw	x,#32
2154  0004 271d          	jreq	L262
2155  0006 a30302        	cpw	x,#770
2156  0009 2718          	jreq	L262
2157  000b a30301        	cpw	x,#769
2158  000e 2713          	jreq	L262
2159  0010 a30210        	cpw	x,#528
2160  0013 270e          	jreq	L262
2161  0015 ae022a        	ldw	x,#554
2162  0018 89            	pushw	x
2163  0019 5f            	clrw	x
2164  001a 89            	pushw	x
2165  001b ae0008        	ldw	x,#L531
2166  001e cd0000        	call	_assert_failed
2168  0021 5b04          	addw	sp,#4
2169  0023               L262:
2170                     ; 557   if (UART3_FLAG == UART3_FLAG_RXNE)
2172  0023 1e01          	ldw	x,(OFST+1,sp)
2173  0025 a30020        	cpw	x,#32
2174  0028 2606          	jrne	L177
2175                     ; 559     UART3->SR = (uint8_t)~(UART3_SR_RXNE);
2177  002a 35df5240      	mov	21056,#223
2179  002e 201a          	jra	L377
2180  0030               L177:
2181                     ; 562   else if (UART3_FLAG == UART3_FLAG_LBDF)
2183  0030 a30210        	cpw	x,#528
2184  0033 2606          	jrne	L577
2185                     ; 564     UART3->CR4 &= (uint8_t)(~UART3_CR4_LBDF);
2187  0035 72195247      	bres	21063,#4
2189  0039 200f          	jra	L377
2190  003b               L577:
2191                     ; 567   else if (UART3_FLAG == UART3_FLAG_LHDF)
2193  003b a30302        	cpw	x,#770
2194  003e 2606          	jrne	L1001
2195                     ; 569     UART3->CR6 &= (uint8_t)(~UART3_CR6_LHDF);
2197  0040 72135249      	bres	21065,#1
2199  0044 2004          	jra	L377
2200  0046               L1001:
2201                     ; 574     UART3->CR6 &= (uint8_t)(~UART3_CR6_LSF);
2203  0046 72115249      	bres	21065,#0
2204  004a               L377:
2205                     ; 576 }
2208  004a 85            	popw	x
2209  004b 81            	ret	
2284                     ; 591 ITStatus UART3_GetITStatus(UART3_IT_TypeDef UART3_IT)
2284                     ; 592 {
2285                     .text:	section	.text,new
2286  0000               _UART3_GetITStatus:
2288  0000 89            	pushw	x
2289  0001 89            	pushw	x
2290       00000002      OFST:	set	2
2293                     ; 593   ITStatus pendingbitstatus = RESET;
2295                     ; 594   uint8_t itpos = 0;
2297                     ; 595   uint8_t itmask1 = 0;
2299                     ; 596   uint8_t itmask2 = 0;
2301                     ; 597   uint8_t enablestatus = 0;
2303                     ; 600   assert_param(IS_UART3_GET_IT_OK(UART3_IT));
2305  0002 a30277        	cpw	x,#631
2306  0005 2731          	jreq	L472
2307  0007 a30266        	cpw	x,#614
2308  000a 272c          	jreq	L472
2309  000c a30255        	cpw	x,#597
2310  000f 2727          	jreq	L472
2311  0011 a30244        	cpw	x,#580
2312  0014 2722          	jreq	L472
2313  0016 a30235        	cpw	x,#565
2314  0019 271d          	jreq	L472
2315  001b a30346        	cpw	x,#838
2316  001e 2718          	jreq	L472
2317  0020 a30412        	cpw	x,#1042
2318  0023 2713          	jreq	L472
2319  0025 a30100        	cpw	x,#256
2320  0028 270e          	jreq	L472
2321  002a ae0258        	ldw	x,#600
2322  002d 89            	pushw	x
2323  002e 5f            	clrw	x
2324  002f 89            	pushw	x
2325  0030 ae0008        	ldw	x,#L531
2326  0033 cd0000        	call	_assert_failed
2328  0036 5b04          	addw	sp,#4
2329  0038               L472:
2330                     ; 603   itpos = (uint8_t)((uint8_t)1 << (uint8_t)((uint8_t)UART3_IT & (uint8_t)0x0F));
2332  0038 7b04          	ld	a,(OFST+2,sp)
2333  003a a40f          	and	a,#15
2334  003c 5f            	clrw	x
2335  003d 97            	ld	xl,a
2336  003e a601          	ld	a,#1
2337  0040 5d            	tnzw	x
2338  0041 2704          	jreq	L003
2339  0043               L203:
2340  0043 48            	sll	a
2341  0044 5a            	decw	x
2342  0045 26fc          	jrne	L203
2343  0047               L003:
2344  0047 6b01          	ld	(OFST-1,sp),a
2346                     ; 605   itmask1 = (uint8_t)((uint8_t)UART3_IT >> (uint8_t)4);
2348  0049 7b04          	ld	a,(OFST+2,sp)
2349  004b 4e            	swap	a
2350  004c a40f          	and	a,#15
2351  004e 6b02          	ld	(OFST+0,sp),a
2353                     ; 607   itmask2 = (uint8_t)((uint8_t)1 << itmask1);
2355  0050 5f            	clrw	x
2356  0051 97            	ld	xl,a
2357  0052 a601          	ld	a,#1
2358  0054 5d            	tnzw	x
2359  0055 2704          	jreq	L403
2360  0057               L603:
2361  0057 48            	sll	a
2362  0058 5a            	decw	x
2363  0059 26fc          	jrne	L603
2364  005b               L403:
2365  005b 6b02          	ld	(OFST+0,sp),a
2367                     ; 610   if (UART3_IT == UART3_IT_PE)
2369  005d 1e03          	ldw	x,(OFST+1,sp)
2370  005f a30100        	cpw	x,#256
2371  0062 260c          	jrne	L7301
2372                     ; 613     enablestatus = (uint8_t)((uint8_t)UART3->CR1 & itmask2);
2374  0064 c65244        	ld	a,21060
2375  0067 1402          	and	a,(OFST+0,sp)
2376  0069 6b02          	ld	(OFST+0,sp),a
2378                     ; 616     if (((UART3->SR & itpos) != (uint8_t)0x00) && enablestatus)
2380  006b c65240        	ld	a,21056
2382                     ; 619       pendingbitstatus = SET;
2384  006e 2020          	jp	LC013
2385                     ; 624       pendingbitstatus = RESET;
2386  0070               L7301:
2387                     ; 627   else if (UART3_IT == UART3_IT_LBDF)
2389  0070 a30346        	cpw	x,#838
2390  0073 260c          	jrne	L7401
2391                     ; 630     enablestatus = (uint8_t)((uint8_t)UART3->CR4 & itmask2);
2393  0075 c65247        	ld	a,21063
2394  0078 1402          	and	a,(OFST+0,sp)
2395  007a 6b02          	ld	(OFST+0,sp),a
2397                     ; 632     if (((UART3->CR4 & itpos) != (uint8_t)0x00) && enablestatus)
2399  007c c65247        	ld	a,21063
2401                     ; 635       pendingbitstatus = SET;
2403  007f 200f          	jp	LC013
2404                     ; 640       pendingbitstatus = RESET;
2405  0081               L7401:
2406                     ; 643   else if (UART3_IT == UART3_IT_LHDF)
2408  0081 a30412        	cpw	x,#1042
2409  0084 2616          	jrne	L7501
2410                     ; 646     enablestatus = (uint8_t)((uint8_t)UART3->CR6 & itmask2);
2412  0086 c65249        	ld	a,21065
2413  0089 1402          	and	a,(OFST+0,sp)
2414  008b 6b02          	ld	(OFST+0,sp),a
2416                     ; 648     if (((UART3->CR6 & itpos) != (uint8_t)0x00) && enablestatus)
2418  008d c65249        	ld	a,21065
2420  0090               LC013:
2421  0090 1501          	bcp	a,(OFST-1,sp)
2422  0092 271a          	jreq	L7601
2423  0094 7b02          	ld	a,(OFST+0,sp)
2424  0096 2716          	jreq	L7601
2425                     ; 651       pendingbitstatus = SET;
2427  0098               LC012:
2431  0098 a601          	ld	a,#1
2434  009a 2013          	jra	L5401
2435                     ; 656       pendingbitstatus = RESET;
2436  009c               L7501:
2437                     ; 662     enablestatus = (uint8_t)((uint8_t)UART3->CR2 & itmask2);
2439  009c c65245        	ld	a,21061
2440  009f 1402          	and	a,(OFST+0,sp)
2441  00a1 6b02          	ld	(OFST+0,sp),a
2443                     ; 664     if (((UART3->SR & itpos) != (uint8_t)0x00) && enablestatus)
2445  00a3 c65240        	ld	a,21056
2446  00a6 1501          	bcp	a,(OFST-1,sp)
2447  00a8 2704          	jreq	L7601
2449  00aa 7b02          	ld	a,(OFST+0,sp)
2450                     ; 667       pendingbitstatus = SET;
2452  00ac 26ea          	jrne	LC012
2453  00ae               L7601:
2454                     ; 672       pendingbitstatus = RESET;
2459  00ae 4f            	clr	a
2461  00af               L5401:
2462                     ; 676   return  pendingbitstatus;
2466  00af 5b04          	addw	sp,#4
2467  00b1 81            	ret	
2504                     ; 706 void UART3_ClearITPendingBit(UART3_IT_TypeDef UART3_IT)
2504                     ; 707 {
2505                     .text:	section	.text,new
2506  0000               _UART3_ClearITPendingBit:
2508  0000 89            	pushw	x
2509       00000000      OFST:	set	0
2512                     ; 709   assert_param(IS_UART3_CLEAR_IT_OK(UART3_IT));
2514  0001 a30255        	cpw	x,#597
2515  0004 2718          	jreq	L613
2516  0006 a30412        	cpw	x,#1042
2517  0009 2713          	jreq	L613
2518  000b a30346        	cpw	x,#838
2519  000e 270e          	jreq	L613
2520  0010 ae02c5        	ldw	x,#709
2521  0013 89            	pushw	x
2522  0014 5f            	clrw	x
2523  0015 89            	pushw	x
2524  0016 ae0008        	ldw	x,#L531
2525  0019 cd0000        	call	_assert_failed
2527  001c 5b04          	addw	sp,#4
2528  001e               L613:
2529                     ; 712   if (UART3_IT == UART3_IT_RXNE)
2531  001e 1e01          	ldw	x,(OFST+1,sp)
2532  0020 a30255        	cpw	x,#597
2533  0023 2606          	jrne	L1111
2534                     ; 714     UART3->SR = (uint8_t)~(UART3_SR_RXNE);
2536  0025 35df5240      	mov	21056,#223
2538  0029 200f          	jra	L3111
2539  002b               L1111:
2540                     ; 717   else if (UART3_IT == UART3_IT_LBDF)
2542  002b a30346        	cpw	x,#838
2543  002e 2606          	jrne	L5111
2544                     ; 719     UART3->CR4 &= (uint8_t)~(UART3_CR4_LBDF);
2546  0030 72195247      	bres	21063,#4
2548  0034 2004          	jra	L3111
2549  0036               L5111:
2550                     ; 724     UART3->CR6 &= (uint8_t)(~UART3_CR6_LHDF);
2552  0036 72135249      	bres	21065,#1
2553  003a               L3111:
2554                     ; 726 }
2557  003a 85            	popw	x
2558  003b 81            	ret	
2571                     	xdef	_UART3_ClearITPendingBit
2572                     	xdef	_UART3_GetITStatus
2573                     	xdef	_UART3_ClearFlag
2574                     	xdef	_UART3_GetFlagStatus
2575                     	xdef	_UART3_SetAddress
2576                     	xdef	_UART3_SendBreak
2577                     	xdef	_UART3_SendData9
2578                     	xdef	_UART3_SendData8
2579                     	xdef	_UART3_ReceiveData9
2580                     	xdef	_UART3_ReceiveData8
2581                     	xdef	_UART3_WakeUpConfig
2582                     	xdef	_UART3_ReceiverWakeUpCmd
2583                     	xdef	_UART3_LINCmd
2584                     	xdef	_UART3_LINConfig
2585                     	xdef	_UART3_LINBreakDetectionConfig
2586                     	xdef	_UART3_ITConfig
2587                     	xdef	_UART3_Cmd
2588                     	xdef	_UART3_Init
2589                     	xdef	_UART3_DeInit
2590                     	xref	_assert_failed
2591                     	xref	_CLK_GetClockFreq
2592                     	switch	.const
2593  0008               L531:
2594  0008 2e2e5c2e2e5c  	dc.b	"..\..\..\..\librar"
2595  001a 6965735c7374  	dc.b	"ies\stm8s_stdperip"
2596  002c 685f64726976  	dc.b	"h_driver\src\stm8s"
2597  003e 5f7561727433  	dc.b	"_uart3.c",0
2598                     	xref.b	c_lreg
2599                     	xref.b	c_x
2619                     	xref	c_lursh
2620                     	xref	c_lsub
2621                     	xref	c_smul
2622                     	xref	c_ludv
2623                     	xref	c_rtol
2624                     	xref	c_llsh
2625                     	xref	c_lcmp
2626                     	xref	c_ltor
2627                     	end
