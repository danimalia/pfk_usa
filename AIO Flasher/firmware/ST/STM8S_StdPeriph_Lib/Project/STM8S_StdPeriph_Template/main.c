/**
  ******************************************************************************
  * @file     TIM4_TimeBase\main.c
  * @author   MCD Application Team
  * @version V2.0.4
  * @date     26-April-2018
  * @brief    This file contains the main function for TIM4 Time Base Configuration example.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
//#include "stm8s_eval.h"
//#define THREE_FLASH
#define EIGHT_FLASH
/**
  * @addtogroup TIM4_TimeBase
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TIM4_PERIOD       124 
#define TOTAL_PERIOD			1300
#define ON_PERIOD					130
#define OFF_PERIOD				130
#define WAIT_PERIOD				380
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
__IO uint32_t TimingDelay = TOTAL_PERIOD;
	uint8_t flash_count = 0;
	uint8_t StatoIn1 = 3;
	uint16_t In1_0 = 0;
	uint16_t In1_1 = 3;
	uint16_t Debounce = 250;
	uint8_t input1 = 0;
	typedef enum { FAILED = 0, PASSED = !FAILED} TestStatus;
	__IO TestStatus OperationStatus;
	
/* Private function prototypes -----------------------------------------------*/
void Delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);
static void CLK_Config(void);
static void TIM4_Config(void);
static void GPIO_Config(void);
static void UpdateLEDs(void);
void UpdateSwitch(void);
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/
/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
void main(void)
{

    uint8_t val = 0x00, val_comp = 0x00;
    uint32_t add = 0x00;

    /* Define FLASH programming time */
    FLASH_SetProgrammingTime(FLASH_PROGRAMTIME_STANDARD);

    /* Unlock Data memory */
    FLASH_Unlock(FLASH_MEMTYPE_DATA);

		flash_count = FLASH_ReadByte(0x40A5);

    /* Pass */
    OperationStatus = PASSED;
    /* OperationStatus = PASSED, if the data written/read to/from DATA EEPROM memory is correct */
    /* OperationStatus = FAILED, if the data written/read to/from DATA EEPROM memory is corrupted */
	
  /* Clock configuration -----------------------------------------*/
  CLK_Config();  

  /* GPIO configuration -----------------------------------------*/
  GPIO_Config();  

  /* TIM4 configuration -----------------------------------------*/
  TIM4_Config();    
  
  while (1)
  {
		UpdateLEDs();
		UpdateSwitch();
  }
}

static void UpdateLEDs(void)
{
	unsigned long currentTick = TimingDelay;
	#ifdef EIGHT_FLASH
	switch (flash_count)
		{
			//double flash alternating 130ms
			case 0: 
			if (TimingDelay == TOTAL_PERIOD) 		GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-130)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-260)) GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-390)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-520)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-650)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-780)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-910)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			break;
			
			//triple flash alternating 50ms
			case 1: 
			if (TimingDelay == TOTAL_PERIOD) 		GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-50)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-100)) GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-150)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-200)) GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-250)) GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-300)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-350)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-400)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-450)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-500)) GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			else if (TimingDelay == (TOTAL_PERIOD-550)) GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
			break;
			
			//single flash alternating 650ms
			case 2: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-650)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
			break;		

			//triple flash 1 output only 50ms
			case 3: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-50)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-100)) 
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-150)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-200)) 
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-250)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				break;
				
				//single flash alternating 90ms
				case 4: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-90)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-180)) TimingDelay = TOTAL_PERIOD;
				break;

				//single flash alternating 150ms
				case 5: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-150)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-300)) TimingDelay = TOTAL_PERIOD;
				break;

				//double flash alternating 75ms 
				case 6: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-75)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-150) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-225)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-300) 		
				{
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-375)) 
				{
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-450) 		
				{
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-525)) 
				{
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-600)) TimingDelay = TOTAL_PERIOD;
				
				break;
				
				//double flash alternating 150ms 				
				case 7: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-150)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-300) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-450)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-600) 		
				{
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-750)) 
				{
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-900) 		
				{
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-1050)) 
				{
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-1200)) TimingDelay = TOTAL_PERIOD;
				break;
				
			break;		
			default: break;		
		}
#endif
#ifdef THREE_FLASH
	switch (flash_count)
	{
			//single flash alternating 650ms
			case 0: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-650)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
			break;
			//single flash alternating 90ms
			case 1: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-90)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-180)) TimingDelay = TOTAL_PERIOD;
				break;
				
			//double flash alternating 150ms 	
			case 2: 
				if (TimingDelay == TOTAL_PERIOD) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-150)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-300) 		
				{
					GPIO_WriteHigh(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-450)) 
				{
					GPIO_WriteLow(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-600) 		
				{
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-750)) 
				{
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				if (TimingDelay == TOTAL_PERIOD-900) 		
				{
					GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-1050)) 
				{
					GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3);
				}
				else if (TimingDelay == (TOTAL_PERIOD-1200)) TimingDelay = TOTAL_PERIOD;
				break;
				
			break;		
			default: break;	
		}		
		#endif
}


void UpdateSwitch(void)
{
	//input1 = GPIO_ReadInputPin(GPIOA, GPIO_PIN_1);
	uint8_t val = 0;
	if (input1 == 0)	
		{
			In1_0++;
			In1_1 = 0;
			if ((In1_0 > Debounce)&& (StatoIn1 == 1))
			{
				In1_0 = Debounce + 1;
				StatoIn1 = 0;
				flash_count++;
				#ifdef EIGHT_FLASH
				if (flash_count >= 8) 
				{
					flash_count = 0;
				}
				#endif
				#ifdef THREE_FLASH
				if (flash_count >= 3) 
				{
					flash_count = 0;
				}
				#endif
				//store flash_count
				FLASH_ProgramByte((0x40A5), flash_count);
				val = FLASH_ReadByte(0x40A5);
			}
		}	
		else 
		{
			In1_0 = 0;
			In1_1++;
			if ((In1_1 > Debounce))
			{
				In1_1 = Debounce + 1;
				StatoIn1 = 1;
			}
		}
				
}
/**
  * @brief  Configure system clock to run at 16Mhz
  * @param  None
  * @retval None
  */
static void CLK_Config(void)
{
    /* Initialization of the clock */
    /* Clock divider to HSI/1 */
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
}

/**
  * @brief  Configure TIM4 to generate an update interrupt each 1ms 
  * @param  None
  * @retval None
  */
static void TIM4_Config(void)
{
  /* TIM4 configuration:
   - TIM4CLK is set to 16 MHz, the TIM4 Prescaler is equal to 128 so the TIM1 counter
   clock used is 16 MHz / 128 = 125 000 Hz
  - With 125 000 Hz we can generate time base:
      max time base is 2.048 ms if TIM4_PERIOD = 255 --> (255 + 1) / 125000 = 2.048 ms
      min time base is 0.016 ms if TIM4_PERIOD = 1   --> (  1 + 1) / 125000 = 0.016 ms
  - In this example we need to generate a time base equal to 1 ms
   so TIM4_PERIOD = (0.001 * 125000 - 1) = 124 */

  /* Time base configuration */
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
  /* Clear TIM4 update flag */
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  /* Enable update interrupt */
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  
  /* enable interrupts */
  enableInterrupts();

  /* Enable TIM4 */
  TIM4_Cmd(ENABLE);
}

/**
  * @brief  Configure GPIO for LEDs available on the evaluation board
  * @param  None
  * @retval None
  */
static void GPIO_Config(void)
{
		GPIO_Init(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST);
		GPIO_Init(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST);
		GPIO_Init(GPIOA, (GPIO_Pin_TypeDef)GPIO_PIN_1, GPIO_MODE_IN_PU_NO_IT);
}

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{
  TimingDelay = nTime;

  //while (TimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0)
  {
    TimingDelay--;
  }	
	else TimingDelay = TOTAL_PERIOD;
	
	input1 = GPIO_ReadInputPin(GPIOA, GPIO_PIN_1);
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
