/**
 ******************************************************************************
 * @file    gnss1a1_conf.h
 * @author  SRA
 * @brief   This file contains definitions for the GNSS components bus interfaces
 ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GNSS1A1_CONF_H
#define GNSS1A1_CONF_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f0xx_hal.h"
#include "custom_bus.h"
#include "stm32_bus_ex.h"
#include "custom_errno.h"

#define USE_I2C 0U

#define USE_GNSS1A1_GNSS_TESEO_LIV3F	1U

#define GNSS1A1_GNSS_UART_Init        BSP_USART1_Init
#define GNSS1A1_GNSS_UART_DeInit      BSP_USART1_DeInit
#define GNSS1A1_GNSS_UART_Transmit_IT BSP_USART1_Send_IT
#define GNSS1A1_GNSS_UART_Receive_IT  BSP_USART1_Recv_IT
#define GNSS1A1_GNSS_GetTick         BSP_GetTick

#define GNSS1A1_GNSS_UART_ClearOREF   BSP_USART1_ClearOREF

#define GNSS1A1_RST_PORT                        GPIOB
#define GNSS1A1_RST_PIN                         GPIO_PIN_0

#define GNSS1A1_WAKEUP_PORT                     GPIOB
#define GNSS1A1_WAKEUP_PIN                      GPIO_PIN_1

//#define GNSS1A1_RegisterDefaultMspCallbacks     BSP_USART1_RegisterDefaultMspCallbacks
#define GNSS1A1_RegisterRxCb                    BSP_USART1_RegisterRxCallback
#define GNSS1A1_RegisterErrorCb                 BSP_USART1_RegisterErrorCallback

/* To be checked */
#define GNSS1A1_UART_IRQHanlder                 BSP_USART1_IRQHanlder

#ifdef __cplusplus
}
#endif

#endif /* GNSS1A1_CONF_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

